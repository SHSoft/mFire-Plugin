--[[
		插件作者	:	山海师、小玮、神梦无痕、xLeaves
		发布时间	:	2016.03.04
		联系 Q Q	:	1915025270
		交流QQ群	:	153263411
		
		本插件稳定每周更新，发现BUG或有新的功能需求请联系作者（联系方式往上看↑_↑）
--]]









--[[
		---------------------------------------------- 初始化库 ----------------------------------------------
--]]

-- 生成优化的随机数种子
math.randomseed(tonumber(tostring(math.sqrt(os.time())):reverse()))
-- 兼容调试环境
if type(LuaAuxLib) == "Table" then
	print = LuaAuxLib.TracePrint
else
	QMPlugin	= {}
	null		= nil
end





--[[
		---------------------------------------------- 全局数据 ----------------------------------------------
--]]

local SH_Ver_Main	= 3.0			-- 对外版本号
local SH_Ver_Last	= 20160607		-- 内部版本号 [即最后更新日期]





--[[
		---------------------------------------------- 基础函数 [导出] ----------------------------------------------
--]]

-- 转换为逻辑值 [按照0为false规则转换]
function tb(var)
	if var then
		if tonumber(var) == 0 then
			return false
		else
			return true
		end
	else
		return false
	end
end
QMPlugin.tb = tb

-- iif
function iif(exp, rtrue, rfalse)
	if tb(exp) then
		return rtrue
	else
		return rfalse
	end
end
QMPlugin.iif = iif

-- 返回当前插件版本号
function Ver(tpe)
	return iif(tpe, SH_Ver_Main, SH_Ver_Last)
end
QMPlugin.Ver = Ver

-- 返回变量的数据类型
QMPlugin.Type = type

-- 导出位运算
if bit32 ~= nil then
	QMPlugin.bAnd = bit32.band
	QMPlugin.bOr = bit32.bor
	QMPlugin.bNot = bit32.bnot
	QMPlugin.Xor = bit32.bxor
	QMPlugin.Shl = bit32.lshift
	QMPlugin.Shr = bit32.rshift
end

-- 等价位运算
function Eqv(t1, t2)
	if type(t1) == "boolean" and type(t2) == "boolean" then
		if t1 == t2 then
			return true
		else
			return false
		end
	else
		local i1, i2, ir
		i1 = tonumber(t1)
		i2 = tonumber(t2)
		ir = 0
		for i=0, 31 do
			local b1 = bit32.band(2^i, i1)
			local b2 = bit32.band(2^i, i2)
			if b1 == b2 then
				ir = bit32.bor(ir, 2^i)
			end
		end
		return ir
	end
end
QMPlugin.Eqv = Eqv

-- 随机数
QMPlugin.Rand = math.random

-- 随机逻辑值 [返回成功或失败]
function RandBool()
	return iif(math.random(2) == 1, true, false)
end
QMPlugin.RandBool = RandBool

-- 万分比几率测试 [c:成功几率] [返回成功或失败]
function RandTest(c)
	return iif(math.random(10000) < c, true, false)
end
QMPlugin.RandTest = RandTest

-- 随机抽签 [arr:要进行抽签的数组] [返回抽到的值,出错返回0]
function RandArray(arr)
	if type(arr) == "table" then
		return arr[math.random(#arr)]
	end
	print("Error : RandArray [arr参数类型仅限数组]")
	return 0			-- 返回一个不至于出错的替代值
end
QMPlugin.RandArray = RandArray

-- 生成指定范围指定数量的不重复数组 [min:随机数最小值, max:随机数最大值, count:生成数量] [返回包含随机数的数组]
function Rands(min, max, count)
	local tmparr = {}
	if (max - min + 1) < count then count = max - min + 1 end
	for i = 1, count do
		while true do
			local rndnum = math.random(min, max)
			if tmparr[rndnum] == nil then tmparr[rndnum] = true; break end
		end
	end
	local i = 0
	local retarr = {}
	for k in pairs(tmparr) do i = i + 1; retarr[i] = k end
	return retarr
end

-- 执行并返回execute命令的结果 [cmd:执行的命令行] [返回结果文本]
function execute(cmd)
	local iRet, sRet = pcall(function()
		local tFile = TempFile()
		if tFile == "" then
			return ""
		else
			cmd = cmd.." > " .. tFile
			os.execute(cmd)
			return ReadFile(tFile, true)
		end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end





--[[
		---------------------------------------------- 字符串函数 [导出] ----------------------------------------------
--]]

-- 过滤前导字符 [str:要处理的字符串, filt:要过滤的字符] [返回处理后的字符串]
function LTrimEx(str, filt)
	local iRet, sRet = pcall(function()
		local retstr = ""
		for i = 1, string.len(str) do
			if string.find(filt, string.sub(str, i, i)) == null then
				retstr = string.sub(str, i, -1)
				break
			end
		end
		return retstr
	end)
	print(iRet, sRet)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return str
	end
end
QMPlugin.LTrimEx = LTrimEx

-- 过滤后导字符 [str:要处理的字符串, filt:要过滤的字符] [返回处理后的字符串]
function RTrimEx(str, filt)
	local iRet, sRet = pcall(function()
		local retstr = ""
		for i = string.len(str), 1, -1 do
			if string.find(filt, string.sub(str, i, i)) == null then
				retstr = string.sub(str, 1, i)
				break
			end
		end
		return retstr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return str
	end
end
QMPlugin.RTrimEx = RTrimEx

-- 过滤前导与后导字符 [str:要处理的字符串, filt:要过滤的字符] [返回处理后的字符串]
function TrimEx(str, filt)
	local tmpstr
	tmpstr = LTrimEx(str, filt)
	return RTrimEx(tmpstr, filt)
end
QMPlugin.TrimEx = TrimEx

-- 删除字符中间的一段 [str:要处理的字符串, sval:删除开始位置, eval:删除结束位置] [返回处理后的字符串]
function DelMid(str, sval, eval)
	local iRet, sRet = pcall(function()
		local LStr = string.sub(str, 1, sval-1)
		local RStr = string.sub(str, eval+1, -1)
		local RetStr = LStr ..RStr
		return RetStr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return str
	end
end
QMPlugin.DelMid = DelMid

-- 删除指定字符 [str:要处理的字符串, filter:要删除的字符]
function FilterStr(str, filter)
	local iRet, sRet = pcall(function()
		local RetStr, TmpLStr, TmpRStr, s, e
		RetStr = str
		while true do
			s, e = string.find(RetStr, filter, e)
			if s~= nil then
				TmpLStr = string.sub(RetStr, 1, s-1)
				TmpRStr = string.sub(RetStr, e+1, -1)
				RetStr = TmpLStr ..TmpRStr
			else
				break
			end
		end
		return RetStr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return "" 
	end
end
QMPlugin.FilterStr = FilterStr

-- 统计指定字符数量 [str:要处理的字符串, substr:要查找的子串]
function CountStr(str, substr)
	local iRet, sRet = pcall(function()
		local count = 0
		for k in string.gmatch(str, substr) do
			count = count +1
		end
		return count
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
QMPlugin.CountStr = CountStr

-- 格式化字符串
function sprintf(fs, ...)
	return string.format(fs, ...)
end
QMPlugin.sprintf = sprintf

-- 格式化字符串输出
function printf(fs, ...)
	print(sprintf(fs, ...))
end
QMPlugin.printf = printf





--[[
		---------------------------------------------- 文件目录函数 [导出] ----------------------------------------------
--]]

-- 读取文件 [path:路径, isdel:是否删除] [返回文件内容, 失败返回空字符串]
function ReadFile(path, isdel)
	local iRet, sRet = pcall(function()
		local f = io.open(path, "r")
		assert(f, "文件无法访问。")
		local ret = f:read("*all")
		f:close()
		if isdel then
			os.remove(path)
		end
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
QMPlugin.ReadFile = ReadFile

-- 按键精灵缓存目录的上级目录 [无参数] [返回临时文件目录路径, 失败返回空字符串]
function TempPath()
	if type(__MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__) == "String" then
		return __MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__
	else
		return ""
	end
end
QMPlugin.TempPath = TempPath

-- 临时文件路径 [fn:文件名] [返回文件路径, 失败返回空字符串]
function TempFile(fn)
	local tPath = TempPath()
	if tPath == "" then
		return ""
	else
		if fn == null then
			return tPath .. "shs_temp.txt"
		else
			return tPath .. fn .. ".txt"
		end
	end
end
QMPlugin.TempPath = TempPath

-- 提取路径中包含的文件后缀 [path:要提取文件后缀的文件路径]
function QMPlugin.GetFileType(path)
	local iRet, sRet = pcall(function()
		s,e = string.find(path, "%.")
		return string.sub(path, s+1 , -1)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 提取路径中包含的文件名 [path:要提取文件后缀的文件路径]
function QMPlugin.GetFileName(path)
	local iRet, sRet = pcall(function()
		path = "/" .. path
		local ret
		for w in string.gmatch(path, "/([^/]+)") do
			ret = w
		end
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 生成随机名称文件 [Path:文件目录, Prefix:文件名前缀, Postfix:文件名后缀, Lenght:文件名长度]
function QMPlugin.GetTempFile(Path, Prefix, Postfix, Lenght)
	local iRet, sRet = pcall(function()
		local RndText, RetText
		for i = 1, Lenght do
			if RndText == nil then
				RndText = math.random(Lenght)
			else
				RndText = RndText .. math.random(Lenght)
			end
		end
		RetText = Path .. Prefix .. RndText .. Postfix
		local f = io.open(RetText, "a+")
		f:close()
		return RetText
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

-- 遍历目录 [path:要遍历的目录, filter:过滤]
function QMPlugin.ScanPath(path, filter)
	local iRet, sRet = pcall(function()
		if string.sub(path, -1, -1) ~= "/"then
			path = path .. "/"
		end 
		local tpath = TempFile("scan")
		os.execute("ls -F " .. path .. " > " .. tpath)
		local f = io.open(tpath, "r")
		local file = {}
		local folder = {}
		for v in f:lines() do
			if string.find(v, "d ") then
				sPos = string.find(v, " ")
				v = string.sub(v, sPos+1, -1)
				table.insert(folder, v)
			elseif string.find(v, "- ") then
				sPos = string.find(v, " ")
				v = string.sub(v, sPos+1, -1)
				table.insert(file, v)
			end 
		end 
		f:close()
		if filter ~= nil then
			return folder
		else
			return file
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 设置文件权限 [path:文件路径, per:权限]
function QMPlugin.Chmod(path, per)
	local iRet, sRet = pcall(function()
		if per == 0 then
			os.execute("chmod 666 " ..path) 
		elseif per == 1 then
			os.execute("chmod 444 " ..path)
		elseif per == 2 then
			os.execute("chmod 777 " ..path)	
		end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 设置文件权限(可以指定具体权限) [path:文件路径, per:权限]
function QMPlugin.ChmodEx(path, per)
	local iRet, sRet = pcall(function()
		return os.execute("chmod " .. per .. " " .. path)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 写内容到指定行 [path:文件路径, line:行数, str:写入内容]
function QMPlugin.WriteFileLine(path, line, str)
	local iRet, sRet = pcall(function()
		local t = {}
		f = io.open(path, "a+")
		for i in f:lines() do
			table.insert(t, i)
		end
		table.insert(t, line, str)
		f:close()
		f = io.open(path, "w")
		for _, v in ipairs(t) do
			f:write(v .. "\r\n")
		end
		f:close()
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 删除指定目录下指定后缀的文件 (小玮)
function QMPlugin.FindFileDelete(findpath, filename)
	local iRet, sRet = pcall(function()
		os.execute(string.format("find %s -name '%s' | xargs rm -rf", findpath, filename))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end





--[[
		---------------------------------------------- 表与数组函数 [导出] ----------------------------------------------
--]]

-- 复制表 [old:旧表] [返回复制表]
function CopyTable(old)
	local NewTable = {}
	for k, v in pairs(old) do
		NewTable[k] = v
	end
	return NewTable
end
QMPlugin.CopyTable = CopyTable

-- 复制数组 [old:旧数组] [返回复制数组]
function CopyArray(old)
	local NewArray = {}
	for k, v in ipairs(old) do
		NewArray[k] = v
	end
	return NewArray
end
QMPlugin.CopyArray = CopyArray

-- 过滤数组 [arr:数组, substr:要过滤掉的字符串, tpe:是包含substr过滤还是不包含过滤] [返回过滤后的数组]
function QMPlugin.Filter(arr, substr, tpe)
	local iRet, sRet = pcall(function()
		local tarr = {}
		for k, v in ipairs(arr) do
			if tpe then
				if string.find(v, substr) ~= nil then
					table.insert(tarr, v)
				end
			else
				if string.find(v, substr) == nil then
					table.insert(tarr, v)
				end
			end
		end
		return tarr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end

-- 数组排序 [arr:数组, comp:是否重构数组索引] [返回排序后的数组]
function QMPlugin.Sort(arr, comp)
	local iRet, sRet = pcall(function()
		local t = {}
		local j = 1
		local tarr = CopyTable(arr)
		table.sort(tarr)
		if comp then
			for i = #tarr, 1, -1 do
				t[j] = tarr[i]
				j = j + 1
			end
			return t
		else
			return tarr
		end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end

-- 删除数组元素 [arr:数组, ipos:删除元素的位置] [返回操作后的数组]
function QMPlugin.Remove(arr, ipos)
	local iRet, sRet = pcall(function()
		tarr = CopyTable(arr)
		table.remove(tarr, ipos+1)
		return tarr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end

-- 插入数组元素 [arr:数组, ipos:删除元素的位置, value:插入的值] [返回操作后的数组]
function QMPlugin.insert(arr, ipos, value)
	local iRet, sRet = pcall(function()
		tarr = MoveTable(arr)
		table.insert(tarr, ipos+1, value)
		return tarr
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end

-- 数组去重 [tbl:数组或表] [返回操作后的数组]
function QMPlugin.RemoveDup(tbl)
	local iRet, sRet = pcall(function()
		local a = {}
		local b = {}
		for _, v in ipairs(tbl) do
			a[v] = 0
		end
		for k, v in pairs(a) do
			table.insert(b, k)
		end
		return b
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end

-- 数组元素改变位置 [arr:数组或表, a:交换位置的第一个元素, b:第二个元素] [返回操作后的数组]
function QMPlugin.ChangePos(arr, a, b)
	local iRet, sRet = pcall(function()
		local tmptbl = arr
		local tmpval = arr[a+1]
		table.remove(tmptbl, a+1)
		table.insert(tmptbl, b+1, tmpval)
		return tmptbl
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return {}
	end
end





--[[
		---------------------------------------------- 设备及系统函数 [导出] ----------------------------------------------
--]]

-- 获取MAC地址
function QMPlugin.GetMAC()
	local iRet, sRet = pcall(function()
		local path = TempFile("MACRecord")
		os.execute("cat /sys/class/net/wlan0/address > " .. path)
		local r = readfile(path)
		return r
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 设置输入法
function QMPlugin.SetIME(pattern)
	if pattern == 0 then		-- 百度小米版
		os.execute("ime set com.baidu.input_mi/.ImeService ")
	elseif pattern == 1 then  	-- 讯飞
		os.execute("ime set com.iflytek.inputmethod/.FlyIME")
	elseif pattern == 2 then	-- 谷歌
		os.execute("ime set com.google.android.inputmethod.pinyin")
	elseif pattern == 3 then	-- 手心
		os.execute("ime set com.xinshuru.inputmethod/.FTInputService")
	elseif pattern == 4 then	-- GO输入法
		os.execute("ime set com.jb.gokeyboard/.GoKeyboard")
	elseif pattern == 5 then	-- 触宝输入法
		os.execute("ime set com.cootek.smartinputv5.tablet/com.cootek.smartinput5.TouchPalIME")
	elseif pattern == 6 then	-- QQ拼音
		os.execute("ime set com.tencent.qqpinyin/.QQPYInputMethodService ")
	elseif pattern == 7 then	-- 百度输入法
		os.execute("ime set com.baidu.input/.ImeService")
	elseif pattern == 8 then	-- 章鱼输入法
		os.execute("ime set com.komoxo.octopusime/com.komoxo.chocolateime.LatinIME")
	elseif pattern ==9 then 	-- 按键精灵输入法
		os.execute("ime set com.cyjh.mobileanjian/.input.inputkb")
	elseif pattern == 10 then	-- 搜狗输入法
		os.execute("ime set com.sohu.inputmethod.sogou/.SogouIME")
	elseif pattern == 11 then	-- 触摸精灵输入法
		os.execute("ime set net.aisence.Touchelper/.IME")
	elseif pattern == 12 then	-- 脚本精灵输入法
		os.execute("ime set com.scriptelf/.IME")
	elseif pattern == 13 then	-- xscript输入法
		os.execute("ime set com.surcumference.xscript/.Xkbd")
	elseif pattern == 14 then	-- TC输入法
		os.execute("ime set com.xxf.tc.Activity/api.input.TC_IME")
	elseif pattern == 15 then	-- 章鱼输入法
		os.execute("ime set com.tongmo.octopus.helper/com.tongmo.octopus.api.OctopusIME")
	elseif pattern == 16 then	-- 触动精灵输入法
		os.execute("ime set com.touchsprite.android/.core.TSInputMethod")
	end
end

-- 安装app
function QMPlugin.Install(path)
	os.execute("pm install -r " .. path)
end

-- 卸载app
function QMPlugin.Uninstall(PackageName)
	os.execute("pm uninstall  " .. PackageName)
end

-- 获取通知栏信息
function QMPlugin.GetNotification(PackageName)
	local iRet, sRet = pcall(function()
		local s,e,pkgname
		local path = TempFile("Notification")
		os.execute("dumpsys notification > " ..path )
		local text = readfile(path,1)
		repeat
			s,e = string.find(text,"pkg=[^ ]+",e)
			if s == nil then 
				return ""
			end 
			pkgname = string.sub(text,s+4,e)
			if pkgname == PackageName then
				s,e = string.find(text,"tickerText=[^\r\n]+",e)
				if s == nil then 
					return ""
				end 
				tickertext = string.sub(text,s+11,e)
				return tickertext
			end 
		until s == null
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

-- 获取外网ip
function QMPlugin.GetIP()
	local iRet, sRet = pcall(function()
		local path  = TempFile("ip")
		os.execute("curl --connect-timeout 8 www.1356789.com > "..path)
		local ret = readfile(path)
		return ret:match("%d+%.%d+%.%d+%.%d+")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--[[获取外网ip地址  [作者：小玮]
function QMPlugin.GetIP()
	return LuaAuxLib.URL_Operation("http://52xiaov.com/getipinfo.php"):match("ip:(.-)<br/>")
end
--]]

--获取当前应用包名与组件名
function QMPlugin.GetTopActivity()
	local iRet, sRet = pcall(function()
		local path = TempFile ("app")
		os.execute("dumpsys activity top >" .. path)
		return string.match(readfile(path),"ACTIVITY ([^ ]+)")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--获取设备中的应用 
function QMPlugin.GetAppList(type)
	local iRet, sRet = pcall(function()
		local path = TempFile ("applist")
		if type == 0 then 
			type = " -3 >"
		elseif type == 1 then
			type = " -s >"
		end 
		os.execute("pm list packages "..type..path)
		local ret ={}
		for k in string.gmatch(readfile(path),"[^\r\n]+") do 
			table.insert(ret,string.sub(k,9,-1))
		end 
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 
--[[获取已安装包名  [作者：小玮]
function QMPlugin.ListPackage(issystem)
	local localpath = temppath().."ListPackage"
	os.execute("pm list package -f > "..localpath)
	local ReadContent = readfile(localpath)
	local systempackage={}
	local userpackage={}
	for i in ReadContent:gmatch("(.-)\n(.-)") do
		local _,_,savepath,packagename = i:find("package:/(.-)/.-=(.+)")
		if savepath == "system" then
			systempackage[#systempackage+1]=packagename
		elseif savepath == "data" then
			userpackage[#userpackage+1]=packagename
		end
	end
	issystem = tostring(issystem)
	if issystem == "true" then
		return systempackage
	elseif issystem == "false" then
		return userpackage
	else
		for _,i in pairs(userpackage) do
			table.insert(systempackage,i)
		end
		return systempackage
	end
end
--]]

--关闭\开启wifi
function QMPlugin.ControlWifi(mode)
	local iRet, sRet = pcall(function()
		if mode == true then
			os.execute("svc wifi enable")
		else 
			os.execute("svc wifi disable")
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--关闭\开启数据流量
function QMPlugin.ControlData(mode)
	local iRet, sRet = pcall(function()
		if mode == true then
			os.execute("svc data enable")
		else 
			os.execute("svc data disable")
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--检测wifi是否开启
function QMPlugin.IsConnectWifi()
	local iRet, sRet = pcall(function()
		local path = TempFile ("wifi")
		os.execute("dumpsys wifi > "..path)
		local f = io.open(path,"r")
		local ret = f:read("*line")
		f:close()
		if string.find(ret,"disabled") == null then
			return true
		else 
			return false
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取安卓系统版本号
function QMPlugin.GetAndroidVer()
	local iRet, sRet = pcall(function()
		local localpath = TempFile ("AndroidVer")
		os.execute(string.format("getprop ro.build.version.release > %s",localpath))
		return readfile(localpath)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--重启手机
function QMPlugin.Reboot()
	local iRet, sRet = pcall(function()
		os.execute("reboot")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--关机
function QMPlugin.ShutDown()
	local iRet, sRet = pcall(function()
		os.execute("reboot -p")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--判断设备是否是虚拟机
function QMPlugin.IsVM()
	local iRet, sRet = pcall(function()
		local path = TempFile ("IsVM") 
		os.execute("cat /proc/cpuinfo > " .. path)
		retinfo = readfile(path)
		s = string.find(ret,"model name")
		if s == nil then
			ret = false
		else
			ret =true
		end 
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--判断充电状态 
function QMPlugin.GetBatteryState()
	local iRet, sRet = pcall(function()
		local state
		local path = TempFile("Battery")
		os.execute("dumpsys battery > " ..path)
		local ret = readfile(path)
		if string.find(ret,"AC powered: true") then
			state = 1
		elseif string.find(ret,"USB powered: true") then
			state = 2
		elseif string.find(ret,"Wireless powered: true") then
			state = 3
		else 
			state = 0
		end 
		return state
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--判断蓝牙是否开启 
function QMPlugin.IsBluetooth()
	local iRet, sRet = pcall(function()
		local path = TempFile("Bluetooth")
		os.execute("getprop > " ..path)
		local ret = readfile(path)
		if string.find(ret,"%[bluetooth.status%]: %[on%]") then
			return true
		else 
			return false
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--指定APP打开网址  [存在部分应用版本不兼容情况]
function QMPlugin.RunUrl(url,ID)
	local iRet, sRet = pcall(function()
		local tmpact 
		if ID == 0 then		--360浏览器
			tmpact = "com.qihoo.browser/.BrowserActivity"
		elseif ID == 1 then	--QQ浏览器
			tmpact = "com.tencent.mtt.x86/.MainActivity"
		elseif ID == 2 then	--海豚浏览器
			tmpact = "com.dolphin.browser.xf/mobi.mgeek.TunnyBrowser.MainActivity"
		elseif ID == 3 then	--欧朋浏览器
			tmpact = "com.oupeng.browser/com.opera.android.OperaMainActivity"
		elseif ID == 4 then	--傲游浏览器
			tmpact = "com.mx.browser/.MxBrowserActivity"
		elseif ID == 5 then	--UC浏览器
			tmpact = "com.UCMobile/com.uc.browser.InnerUCMobile"
		end 
		os.execute(string.format("am start -n %s -d %s",tmpact,url))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--判断设备中是否有安装指定app
function QMPlugin.ExistApp(pkgname)
	local iRet, sRet = pcall(function()
		local path = TempFile("applist") 
		os.execute("pm list packages  > "..path)
		local ret = readfile(path)
		s = string.find(ret,pkgname)
		if s == nil then
			return false
		else 
			return true 
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--设置手机时间
function QMPlugin.SetTime(d,t)
	local iRet, sRet = pcall(function()
		os.execute("date -s "..d.."."..t)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--隐藏app
function QMPlugin.DisableApp(pkgname)
	local iRet, sRet = pcall(function()
		os.execute("pm disable "..pkgname)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--显示app
function QMPlugin.EnableApp(pkgname)
	local iRet, sRet = pcall(function()
		os.execute("pm enable  "..pkgname)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--检测虚拟键高度 
function QMPlugin.GetNavigationBar()
	local iRet, sRet = pcall(function()
		local path = TempFile("bar")
		os.execute("dumpsys window windows > "..path)
		local text = readfile(path)
		s = string.find(text,"NavigationBar")
		if s ~= nil then
			s = string.find(text,"mFrame",s)
			_,_,h,h1 = string.find(text,"mFrame=%[%d+,(%d+)%]%[%d+,(%d+)%]",s)
			return h1-h
		else 
			return null
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取屏幕分辨率 
function QMPlugin.GetScreen()
	local iRet, sRet = pcall(function()
		local path = TempFile("screen")
		os.execute("dumpsys window displays > "..path)
		local text = readfile(path)
		local ret = {}
		_,_,ret[1],ret[2],ret[3] = string.find(text,"init=(%d+)x(%d+) (%d+)dpi")
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取剩余内存百分比 
function QMPlugin.GetRAM()
	local iRet, sRet = pcall(function()
		local Total,Free
		local path = TempFile("RAM")
		os.execute("dumpsys meminfo > "..path)
		local text = readfile(path)
		_,_,Total = string.find(text,"Total RAM: (%d+)")
		_,_,Free = string.find(text,"Free RAM: (%d+)")
		return string.format("%d",(Free/Total)*100)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取已安装应用的版本号  [作者：小玮]
function QMPlugin.AppVersion(packagename)
	local iRet, sRet = pcall(function()
		local localpath = temppath().."AppVersion"
		os.execute(string.format("dumpsys package %s > %s",packagename,localpath))
		return readfile(localpath):match("versionName=(.-)\n")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--获取已安装应用首次安装的时间  [作者：小玮]
function QMPlugin.AppFirstInstallTime(packagename)
	local iRet, sRet = pcall(function()
		local localpath = temppath().."AppVersion"
		os.execute(string.format("dumpsys package %s > %s",packagename,localpath))
		return readfile(localpath):match("firstInstallTime=(.-)\n")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--获取已安装应用升级安装的时间  [作者：小玮]
function QMPlugin.AppLastUpdateTime(packagename)
	local iRet, sRet = pcall(function()
		local localpath = temppath().."AppVersion"
		os.execute(string.format("dumpsys package %s > %s",packagename,localpath))
		return readfile(localpath):match("lastUpdateTime=(.-)\n")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--发送广播强制刷新指定目录下的图片到图库展示  [作者：小玮]
function QMPlugin.UpdatePicture(picturepath)
	local iRet, sRet = pcall(function()
		os.execute("am broadcast -a android.intent.action.MEDIA_MOUNTED -d file://"..picturepath)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--是否亮屏(原理是检测光感是否开启)  [作者：小玮]
function QMPlugin.IsScreenOn()
	local iRet, sRet = pcall(function()
		local localpath = temppath().."IsScreenOn"
		os.execute("dumpsys power > "..localpath)
		return readfile(localpath):match("mLightSensorEnabled=(.-) ")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--开启飞行模式  [作者：小玮]
function QMPlugin.OpenAirplane()
	local iRet, sRet = pcall(function()
		os.execute("settings put global airplane_mode_on 1")
		os.execute("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--关闭飞行模式  [作者：小玮]
function QMPlugin.CloseAirplane()
	local iRet, sRet = pcall(function()
		os.execute("settings put global airplane_mode_on 0")
		os.execute("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--获取是否自动调节亮度 
function QMPlugin.IsAutoBrightness()
	local iRet, sRet = pcall(function()
		local path = TempFile("AutoBrightness")
		os.execute("settings get system screen_brightness_mode > "..path)
		local ret = readfile(path)
		if ret == 1 then
			return true
		elseif  ret == 0 then
			return false
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--设置自动调节亮度 
function QMPlugin.SetAutoBrightness(mode)
	local iRet, sRet = pcall(function()
		if mode == 0 then --关闭自动调节亮度
			os.execute("settings put system screen_brightness_mode 0")
		elseif mode == 1 then
			os.execute("settings put system screen_brightness_mode 1")
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取当前屏幕亮度 
function QMPlugin.GetBrightness()
	local iRet, sRet = pcall(function()
		local path = TempFile("Brightness")
		os.execute("settings get system screen_brightness > "..path)
		local ret = readfile(path)
		return tonumber(ret)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--设置当前屏幕亮度  
function QMPlugin.SetBrightness(val)
	local iRet, sRet = pcall(function()
		os.execute("settings put system screen_brightness " .. val)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取屏幕休眠时间 
function QMPlugin.GetScreenSleep()
	local iRet, sRet = pcall(function()
		local path = TempFile("ScreenSleep")
		os.execute("settings get system screen_off_timeout > "..path)
		local time = tonumber(readfile(path))
		return time/1000
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--设置屏幕休眠时间 
function QMPlugin.SetScreenSleep(val)
	local iRet, sRet = pcall(function()
		os.execute("settings put system screen_off_timeout "..val*1000)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--设置隐藏\显示虚拟键 [更新]
function QMPlugin.SetNavigationBar(mode)
	local iRet, sRet = pcall(function()
		Mount("/system")
		if mode == true then
			mode = 0
		elseif mode == false then
			mode =1
		end 
		os.execute("chmod 777 /system/build.prop")
		f=io.open("/system/build.prop","a")
		local text = f:read("*a")
		if string.find(text,"qemu%.hw%.mainkeys") ~=nil then
			string.gsub(text,"qemu%.hw%.mainkeys=%w","qemu%.hw%.mainkeys="..mode)
		else 
			f:write("\nqemu.hw.mainkeys=1")
		end 
		if string.find(text,"qemu%.hw%.mainkeys="..mode) ~=nil then 
			return true 
		else 
			return false 
		end 
		f:close()
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

---------------------------------------		HTTP请求	---------------------------------------
--读取文件转换成base64编码
function QMPlugin.ReadFileBase(path)
	local iRet, sRet = pcall(function()
		f = io.open(path,"rb")
		if f == null then 
			return null 
		end 
		bytes = f:read("*all")
		f:close()
		local key='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
		return ((bytes:gsub('.', function(x) 
			local r,key='',x:byte()
			for i=8,1,-1 do r=r..(key%2^i-key%2^(i-1)>0 and '1' or '0') end
			return r;
		end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
			if (#x < 6) then return '' end
			local c=0
			for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
			return key:sub(c+1,c+1)
		end)..({ '', '==', '=' })[#bytes%3+1])
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--GetHttp [可自定义头信息]
function QMPlugin.GetHttp(url, t,header)
	local iRet, sRet = pcall(function()
		local path = TempFile("GET") 
		if t == null then t = 8 end 
		if header == nil then
			os.execute(string.format("curl --connect-timeout %s  -o %s '%s' ",t,path,url))
		else
			os.execute(string.format("curl --connect-timeout %s -H %s -o %s '%s'",t,header,path,url))
		end 
		result = readfile(path,1)
		return result
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--GetHttp 下载文件
function QMPlugin.GetHttpFile(url, filepath, header)
	local iRet, sRet = pcall(function()
		if header == null then
			os.execute(string.format("curl -o %s '%s' ",filepath, url))
		else
			os.execute(string.format("curl -o %s -H %s '%s' ",filepath, header,url))
		end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--PostHttp [可自定义头信息]
function QMPlugin.PostHttp(url,post, t,header)
	local iRet, sRet = pcall(function()
		local path = TempFile("POST") 
		if t == null then t = 8 end 
		if header == null then
			os.execute(string.format("curl -o %s -d '%s' --connect-timeout %s '%s'",path,post,t,url))
		else 
			os.execute(string.format("curl -o %s --data-urlencode '%s' --connect-timeout %s -H %s '%s'",path,post,t,header,url))
		end 
		result = readfile(path)
		return result
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--Post提交信息，可附带cookie信息
function QMPlugin.PostHttpC(url,post,cookie_path ,t,header)
	local iRet, sRet = pcall(function()
		local path = TempFile("POST")
		if t == null then t = 8 end 
		if header == null then
			os.execute(string.format("curl -o %s -d '%s' -b '%s' --connect-timeout %s '%s'",path,post,cookie_path,t,url))
		else 
			os.execute(string.format("curl -o %s -d '%s' -b '%s' --connect-timeout %s -H % '%s'",path,post,cookie_path,t,header,url))
		end 
		result = readfile(path)
		return result
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--GET提交信息，可附带cookie信息
function QMPlugin.GetHttpC(url,cookie_path ,t,header)
	local iRet, sRet = pcall(function()
		local path = TempFile("GET") 
		if t == null then t = 8 end 
		if header == null then
			os.execute(string.format("curl -o %s -b '%s' --connect-timeout %s '%s'",path,cookie_path,t,url))
		else 
			os.execute(string.format("curl -o %s -b '%s' --connect-timeout %s -H % '%s'",path,cookie_path,t,header,url))
		end 
		result = readfile(path)
		return result
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

---------------------------------------	LUA正则模式匹配	---------------------------------------

--全局正则匹配[包含单子串]
function QMPlugin.RegexFind(str,pattern)
	local iRet, sRet = pcall(function()
		local ret ={}
		for v in string.gmatch(str,pattern) do 
			table.insert(ret,v)
		end 
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--全局正则匹配多子串 
function QMPlugin.RegexFindEx(str,pattern)
	local iRet, sRet = pcall(function()
		local t1 = {}
		local i=1
		local ePos
		repeat
			local ta = {string.find(str, pattern, ePos)}
			ePos = ta[2]
			if ta[1] ~= nil then
				t1[i] = ta
				table.remove(t1[i],1)
				table.remove(t1[i],1)
				i=i+1
			end 
		until ta[1]==nil
		return t1
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

---------------------------------------	其他命令	---------------------------------------

--区域颜色一定时间是否变化
function QMPlugin.IsDisplayChange(x,y,x1,y1,t,delay)
	local iRet, sRet = pcall(function()
		local PicPath,t1,intx,inty
		PicPath = __MQM_RUNNER_LOCAL_PATH_GLOBAL_NAME__ .."TmpPic.png"
		t1 = LuaAuxLib.GetTickCount()
		--截取指定范围图片并保存
		LuaAuxLib.SnapShot(PicPath,x,y,x1,y1)
		--在指定的时间内循环找图
		while (LuaAuxLib.GetTickCount() - t1) < t*1000 do 
			LuaAuxLib.KeepReleaseScreenSnapshot(true)
			intx = LuaAuxLib.FindPicture(x-10,y-10,x1+10,y1+10,PicPath,"101010",0,0.8)
			if intx == -1 then 
				LuaAuxLib.KeepReleaseScreenSnapshot(false)
				return true 
			end 
			LuaAuxLib.Sleep(delay*1000)
		end 
		LuaAuxLib.KeepReleaseScreenSnapshot(false)
		return false 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 


--设置日志文件路径 
local log = {}
function QMPlugin.LogPath(path)
	log.path = path
end 
--日志记录 
function QMPlugin.OutLog(msg)
	local iRet, sRet = pcall(function()
		local t = os.date("%H:%M:%S")
		local f = io.open(log.path,"a+")
		f:write(t.. "-\t"..msg.."\r\n")
		f:close()
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--挂载系统权限
function Mount(path)
	local iRet, sRet = pcall(function()
		local localpath, text, tmppath,tmpret
		local a = function()
			localpath = TempFile("mount")
			os.execute("mount > "..localpath)
			text = readfile(localpath)
			tmppath,tmpret = string.match(text,"\n([^ ]+ "..path..")([^,]+)") 
		end 	
		a()
		os.execute("mount -o rw,remount "..tmppath)
		a()
		if string.find(tmpret,"rw") then
			return true
		else 
			return false
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 
QMPlugin.Mount = Mount
--[[挂载/system目录为读写，无参，返回挂载点  [作者：小玮]
function QMPlugin.mount()
	os.remove("/data/system.txt")
	os.execute("mount|grep system > /data/system.txt")
	f = io.open("/data/system.txt", "r") 
	xw = f:read("*all") 
	f:close()
	xw = string.sub(xw,1,string.find(xw," "))
	os.execute("su -c 'mount -o rw "..xw.." /system'")
	return xw
end
--]]

--获取XML文件 
function QMPlugin.GetUIXml()
	local iRet, sRet = pcall(function()
		os.execute("uiautomator dump ")
		local ret = readfile("/sdcard/window_dump.xml")
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--定时器 
local t={} 
function QMPlugin.TimeSign(id)
	t[id] = os.time()
end 
function QMPlugin.Timer(id,diff)
	local iRet, sRet = pcall(function()
		local t1 =os.time()
		if t1-t[id] >= diff then
			t[id] = os.time()
			return true
		else 
			return false
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--清理后台,参数:白名单(不清理应用)，table类型  [作者：小玮]
function QMPlugin.KillClean(pgknamearr)
	local iRet, sRet = pcall(function()
		local localpath = temppath().."list"
		local localpath1 = temppath().."ps"
		os.execute("ls /data/app/ > "..localpath)
		os.execute("ps > "..localpath1)
		local f
		ReadContent = readfile(localpath)
		ReadContent,_ = ReadContent:gsub("-[12]%.apk","")
		f = io.open(localpath,"w")
		f:write(ReadContent)
		f:close()
		ReadContent = readfile(localpath1)
		for l in io.lines(localpath) do
			n=0
			if string.find(ReadContent,l) then
				for i, v in ipairs(pgknamearr) do
					if v == l then
						n=1
					break
					end
				end
				if n==0 then
					os.execute("am force-stop "..l)
				end
			end
		end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--清理应用缓存  [作者：小玮]
function QMPlugin.AppClean(packagename)
	local iRet, sRet = pcall(function()
		os.execute(string.format("pm clear %s",packagename))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--Base64加密Encoding  [作者：小玮]
function QMPlugin.Base64En(data)
	local iRet, sRet = pcall(function()
		local key='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
		return ((data:gsub('.', function(x) 
			local r,key='',x:byte()
			for i=8,1,-1 do r=r..(key%2^i-key%2^(i-1)>0 and '1' or '0') end
			return r;
		end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
			if (#x < 6) then return '' end
			local c=0
			for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
			return key:sub(c+1,c+1)
		end)..({ '', '==', '=' })[#data%3+1])
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--Base64解密Decoding  [作者：小玮]
function QMPlugin.Base64De(data)
	local iRet, sRet = pcall(function()
		local key='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
		data = string.gsub(data, '[^'..key..'=]', '')
		return (data:gsub('.', function(x)
			if (x == '=') then return '' end
			local r,f='',(key:find(x)-1)
			for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
			return r;
		end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
			if (#x ~= 8) then return '' end
			local c=0
			for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
			return string.char(c)
		end))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--获取用户点击屏幕坐标
--参数为横向分辨率,纵向分辨率,扫描周期
--返回值为一个数组，第一个是x坐标，第二个是y坐标  [作者：小玮]
function QMPlugin.Coordinate(ScreenX,ScreenY,Time)
	local iRet, sRet = pcall(function()
		local localpath = temppath().."Coordinate"
		os.execute("getevent -pl>"..localpath)
		file=io.open(localpath, "r+")
		data=file:read("*l")
		while data~=nil do
			data=file:read("*l")
			if data~=nil then
				find=nil
				_,_,find=data:find("ABS_MT_POSITION_X.*max%s+(%d*)")
					if find~=nil then
						maxx=find
					end
				find=nil
				_,_,find=data:find("ABS_MT_POSITION_Y.*max%s+(%d*)")
				if find~=nil then
					maxy=find
				end
			end
		end
		times=tostring(times)
		os.execute("getevent -l -c "..Time..">"..localpath)
		file=io.open(localpath, "r+");
		data=file:read("*l")
		while data~=nil do
			data=file:read("*l")
			if data~=nil then
				if data:find("ABS_MT_POSITION_X")~=nil then
					x= math.floor(tonumber(data:sub(59,62),16)*ScreenX/maxx)
				elseif data:find("ABS_MT_POSITION_Y")~=nil then
					y= math.floor(tonumber(data:sub(59,62),16)*ScreenY/maxy)
				end
			end
		end
		os.remove(localpath)
		local C = {}
		C[1] = x
		C[2] = y
		return C
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

local function digit_to(n,s)
	local iRet, sRet = pcall(function()
		assert(type(n)=="number", "arg #1 error: need a number value.")
		assert(type(s)=="string", "arg #2 error: need a string value.")
		assert((#s)>1, "arg #2 error: too short.")
		local fl = math.floor
		local i = 0
		while 1 do
			if n>(#s)^i then
				i = i + 1
			else
				break
			end
		end
		local ret = ""
		while i>=0 do
			if n>=(#s)^i then
				local tmp = fl(n/(#s)^i)
				n = n - tmp*(#s)^i
				ret = ret..s:sub(tmp+1, tmp+1)
			else
				if ret~="" then
					ret = ret..s:sub(1, 1)
				end
			end
			i = i - 1
		end
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
local function to_digit(ns,s)
	local iRet, sRet = pcall(function()
		assert(type(ns)=="string", "arg #1 error: need a string value.")
		assert(type(s)=="string", "arg #2 error: need a string value.")
		assert((#s)>1, "arg #2 error: too short.")
		local ret = 0
		for i=1,#ns do
			local fd = s:find(ns:sub(i,i))
			if not fd then
				return nil
			end
			fd = fd - 1
			ret = ret + fd*((#s)^((#ns)-i))
		end
		return ret
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
local function s2h(str,spacer)return (string.gsub(str,"(.)",function (c)return string.format("%02x%s",string.byte(c), spacer or"")end))end
local function h2s(h)return(h:gsub("(%x%x)[ ]?",function(w)return string.char(tonumber(w,16))end))end
--unicode转utf8  [作者：小玮]
function QMPlugin.Unicode2Utf8(us)
	local iRet, sRet = pcall(function()
		local u16p = {
			0xdc00,
			0xd800,
		}
		local u16b = 0x3ff
		local u16fx = ""
		local padl = {
			["0"] = 7,
			["1"] = 11,
			["11"] = 16,
			["111"] = 21,
		}
		local padm = {}
		for k,v in pairs(padl) do
			padm[v] = k
		end
		local map = {7,11,16,21}
		return (string.gsub(us, "\\[Uu](%x%x%x%x)", function(uc)
			local ud = tonumber(uc,16)
			for i,v in ipairs(u16p) do
				if ud>=v and ud<(v+u16b) then
					ud = ud - v + (i-1) * 0x40
					if (i-1)>0 then
						u16fx = digit_to(ud, "01")
						return ""
					end
					local bi = digit_to(ud, "01")
					uc = string.format("%x", to_digit(u16fx..string.rep("0",10-#bi)..bi,"01"))
					u16fx = ""
					ud = tonumber(uc,16)
					break
				end
			end
			local bins = digit_to(ud,"01")
			local pads = ""
			for _,i in ipairs(map) do
				if #bins<=i then
					pads = padm[i]
					break
				end
			end
			while #bins<padl[pads] do
				bins = "0"..bins
			end
			local tmp = ""
			if pads~="0" then
				tmp = bins
				bins = ""
			end
			while #tmp>0 do
				bins = "10"..string.sub(tmp, -6, -1)..bins
				tmp = string.sub(tmp, 1, -7)
			end
			return (string.gsub(string.format("%x", to_digit(pads..bins, "01")), "(%x%x)", function(w)
				return string.char(tonumber(w,16))
			end))
		end))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--utf8转unicode  [作者：小玮]
function QMPlugin.Utf82Unicode(s, upper)
	local iRet, sRet = pcall(function()
		local uec = 0
		if upper then
			upper = "\\U"
		else
			upper = "\\u"
		end
		local loop1 = string.gsub(s2h(s), "(%x%x)", function(w)
			local wc = tonumber(w,16)
			if wc>0x7F then
				if uec>0 then
					uec = uec - 1
					if uec==0 then
						return w.."/"
					end
					return w
				end
				local bi = digit_to(wc, "01")
				bi = string.sub(bi, 2, -1)
				while string.sub(bi, 1, 1)=="1" do
					bi = string.sub(bi, 2, -1)
					uec = uec + 1
				end
				return "u/"..w
			else
				if uec>0 then
					uec = 0
					return w.."/"
				end
			end
			return w
		end)
		local u16p = {
			0xdc00,
			0xd800,
		}
		local u16id = 0x10000
		local loop2 = string.gsub(loop1, "u/(%x%x*)/", function(w)
			local wc = tonumber(w,16)
			local tmp
			local bi = digit_to(wc, "01")
			tmp = ""
			while #bi>8 do
				tmp = string.sub(bi, -6, -1)..tmp
				bi = string.sub(bi, 1, -9)
			end
			bi = bi..tmp
			while string.sub(bi, 1, 1)=="1" do
				bi = string.sub(bi, 2, -1)
			end
			wc = to_digit(bi, "01")
			if (wc>=u16id) then
				wc = wc - u16id
				tmp = digit_to(wc, "01")
				tmp = string.rep("0", 20-#tmp)..tmp
				local low = to_digit(string.sub(tmp, -10, -1), "01") + u16p[1]
				local high = to_digit(string.sub(tmp, 1, -11), "01") + u16p[2]
				tmp = string.format("%4x", low)
				return s2h(upper..string.format("%4x", high)..upper..string.format("%4x", low))
			end
			local h = string.format("%x", wc)
			if (#h)%2~=0 then
				h = "0"..h
			end
			return s2h(upper..h)
		end)
		return h2s(loop2)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--日期转换成时间戳  [作者：小玮]
function QMPlugin.ToTime(Date,format)
	local iRet, sRet = pcall(function()
		local Year,Month,Day,Hour,Min,Sec
		Time = {}
		_,_,Year,Month,Day,Hour,Min,Sec = Date:find(format)
		Time.year=Year
		Time.month=Month
		Time.day=Day
		Time.hour=Hour
		Time.min = Min
		Time.sec=Sec
		return os.time(Time)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--秒数转换为天数  [作者：小玮]
function QMPlugin.SecToDay(Sec)
	local iRet, sRet = pcall(function()
		local Day,Hour,Min = 0,0,0
		Sec = tonumber(Sec)
		for i =1,Sec/60 do
			Min = Min + 1
			if Min == 60 then Min = 0 Hour = Hour + 1 end
			if Hour == 24 then Hour = 0 Day = Day + 1 end
		end
		Sec=Sec%60
		return Day.."天"..Hour.."小时"..Min.."分"..Sec.."秒"
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--用微信浏览器打开网页  [作者：小玮]
function QMPlugin.WeiXinUrl(packagename,url)
	local iRet, sRet = pcall(function()
		os.execute(string.format("am start -n %s/.plugin.webview.ui.tools.WebViewUI -d '%s'",packagename,url))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--用默认浏览器打开网页  [作者：小玮]
function QMPlugin.OpenWeb(url)
	local iRet, sRet = pcall(function()
		if url:find("http://") == nil then url = "http://"..url end
		os.execute(string.format("am start -a android.intent.action.VIEW -d "..url))
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--两点经纬度之间的直线距离  [作者：小玮]
function QMPlugin.GetDistance(lat1,lng1,lat2,lng2)
	local iRet, sRet = pcall(function()
		local rad = function(d) return d*math.pi/180 end
		local radLat1 = rad(lat1)
		local radLat2 = rad(lat2)
		return math.floor(2*math.asin(math.sqrt(math.pow(math.sin((radLat1-radLat2)/2),2)+math.cos(radLat1)*math.cos(radLat2)*math.pow(math.sin((rad(lng1) - rad(lng2))/2),2)))*6378.137*10000)/10000
		--地球是一个近乎标准的椭球体,此处地球半径我们选择的是6378.137km
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--对比是否到期,参数：到期时间的年、月、日、时、分、秒,返回值：到期返回-1,获取网络时间失败返回null,未到期返回距离到期剩余的时间(单位秒)  [作者：小玮]
function QMPlugin.CompareTime(Year,Month,Day,Hour,Min,Sec)
	local iRet, sRet = pcall(function()
		local NowNetTime = LuaAuxLib.URL_Operation("http://52xiaov.com/getipinfo.php"):match("秒数%):(.-)</body>")
		if NowNetTime == nil then return null else NowNetTime = tonumber(NowNetTime) end
		local Time = {};Time.year=Year;Time.month=Month;Time.day=Day;Time.hour=Hour;Time.min = Min;Time.sec=Sec
		local ExpireTime = os.time(Time)
		if NowNetTime > ExpireTime then return -1 else return (ExpireTime - NowNetTime) end
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--计算几天后的日期  [作者：小玮] 
function QMPlugin.LateTime(late,Year,Month,Day)
	local iRet, sRet = pcall(function()
		local starttime --
		if Day ~= nil then
			local Time = {};Time.year=Year;Time.month=Month;Time.day=Day
			starttime = os.time(Time)
		else
			starttime = os.time()
		end
		return os.date("%Y-%m-%d",starttime + tonumber(late) * 24 * 60 * 60)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--获取通话状态，返回值为0，表示待机状态、1表示来电未接听状态、2表示电话占线状态  [作者：小玮]
function QMPlugin.CallState()
	local iRet, sRet = pcall(function()
		local localpath = temppath().."CallState"
		os.execute("dumpsys telephony.registry > "..localpath)
		return readfile(localpath):match("mCallState=(.-)\n")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end
--获取来电号码  [作者：小玮]
function QMPlugin.CallIncomingNumber()
	local iRet, sRet = pcall(function()
		local localpath = temppath().."CallIncomingNumber"
		os.execute("dumpsys telephony.registry > "..localpath)
		return readfile(localpath):match("mCallIncomingNumber=(.-)\n")
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end

--执行shell命令 
function QMPlugin.Execute(cmd)
	local iRet, sRet = pcall(function()
		os.execute(cmd)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取网络时间 
function QMPlugin.GetNetTime()
	local iRet, sRet = pcall(function()
		local localpath = temppath().."time"
		os.execute("curl --connect-timeout 10 -I www.taobao.com  > "..localpath)
		local timetext = readfile(localpath)
		local Time = {}
		if timetext ==nil then
			return "TIMEOUT"
		end 
		_,_,_,day,mouth,year,hour,min,sec = string.find(timetext,"Date: (%a+), (%d+) (%a+) (%d+) (%d+):(%d+):(%d+)")
		Time = {["Jan"]=1,["Feb"]=2,["Mar"]=3,["Apr"]=4,["May"]=5,["Jun"]=6,["Jul"]=7,["Aug"]=8,["Sep"]=9,["Oct"]=10,["Nov"]=11,["Dec"]=12}
		if year == nil then 
			return null
		else 
			return string.format("%s/%s/%s %s:%s:%s",year,Time[mouth],day,tonumber(hour)+8,min,sec)
		end 
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 



--获取系统开机时间 [更新]
function QMPlugin.GetUpTime()
	local iRet, sRet = pcall(function()
		local rettime = execute("cat /proc/uptime ")
		_,_,rettime = string.find(rettime,"([^ ]+)")
		return tonumber(rettime)
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

--获取系统上输入法 [更新]
function QMPlugin.GetIME()
	local iRet, sRet = pcall(function()
		local retstr = execute("ime list -s")
		local retIME = {}
		for v in string.gmatch(retstr,"([^\n]+)") do 
			table.insert(retIME,v)
		end 
		return retIME
	end)
	if iRet == true then
		return sRet
	else
		print(sRet)
		return ""
	end
end 

