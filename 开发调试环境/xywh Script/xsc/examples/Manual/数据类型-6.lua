print(10 .. 20)			--> 1020

str = "16"
n = tonumber(str)		-- 将字符串转换为数字
if n == nil then
    error(str .. "无法转换为数字！")
else
    print(n*2)
end

print(tostring(10) == "10")     --> true
print(10 .. "" == "10")         --> true