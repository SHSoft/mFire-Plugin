x = 10
local i = 1			-- 局部变量

while i<=x do
    local x = i*2	-- while 代码块内的局部变量
    print(x)		--> 2, 4, 6, 8, ...
    i = i + 1
end

if i > 20 then
    local x			-- then 代码块内的局部变量
    x = 20
    print(x + 2)
else
    print(x)		--> 10 (输出全局变量)
end

print(x)			--> 10 (输出全局变量)