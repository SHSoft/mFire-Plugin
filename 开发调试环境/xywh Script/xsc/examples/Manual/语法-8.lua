a = 2
b = 3
c = 4
do
    local a2 = 2*a
    local d = (b^2 - 4*a*c)
    x1 = (-b + d)/a2
    x2 = (-b - d)/a2
end				-- 'a2' 与 'd' 的作用范围到此为止

print(x1, x2)