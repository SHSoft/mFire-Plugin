UserMain = "qwer"					-- 注册用户名前缀
UserPsWd = "qweasd"					-- 注册密码
UserName = "yefei"					-- 注册姓名
UserDate = "1990/01/16"				-- 注册生日
Usertag1 = "a"						-- 问题1
Usertag2 = "b"						-- 回答1
Usertag3 = "c"						-- 问题2
Usertag4 = "d"						-- 回答2

SleepTime = 20						-- 输入等待间隔
TimeOut = 5							-- 注册监听超时时间[秒]


regid = 1

function Regedit(RegAddStr)
	MainHWND = findwindow("帐号注册")
	if MainHWND~=0 then
		l,r = getwindowrect(MainHWND)
		-- print(l .. "|" ..  r )
		l = l + 90
		r = r + 56
		flashclick(l,r)
		-- clickpoint(l,r)
		sendkey(UserMain .. RegAddStr .. "	")
		sleep(SleepTime)
		sendkey(UserPsWd .. "	")
		sleep(SleepTime)
		sendkey(UserPsWd .. "	")
		sleep(SleepTime)
		sendkey(UserDate .. "	")
		sleep(SleepTime)
		sendkey(UserName .. "	")
		sleep(SleepTime)
		sendkey(Usertag1 .. "	")
		sleep(SleepTime)
		sendkey(Usertag2 .. "	")
		sleep(SleepTime)
		sendkey(Usertag3 .. "	")
		sleep(SleepTime)
		sendkey(Usertag4 .. "	")
		sleep(SleepTime)
		sendkey(" ")
		sleep(500)
		if iswindowvisible(MainHWND) == 0 then
			OK = waitwindow(1,"提示信息",TimeOut)
			if OK==-1 then
				Temp = findwindow("提示信息")
				--msgbox(getwindowtext(getenumwindow(Temp,2)))
				if getwindowtext(getenumwindow(Temp,2))=="您的帐号创建成功.\r\n请妥善保管您的帐号和密码,\r\n并且不要因任何原因把帐号和密码告诉任何其他人.\r\n如果忘记了密码,您可以通过我们的主页重新找回." then
					print(UserMain .. RegAddStr .. "注册成功！")
					regid = regid + 1
				elseif getwindowtext(getenumwindow(Temp,2))=="" then
					print(UserMain .. RegAddStr .. "已经被注册！")
					regid = regid + 1
				else
					print(UserMain .. RegAddStr .. "未知原因注册失败，将再次尝试注册！")
				end
			else
				print(UserMain .. RegAddStr .. "等待超时，将再次尝试注册！")
			end
		else
			print(UserMain .. RegAddStr .. "输入错误，将再次尝试注册！")
		end
	else
		print("可能注册窗口没有打开，无法注册!")
	end
end






print("按下 F9 自动注册一个账号。")
print("按下 + 注册号码加一。")
print("按下 - 注册号码减一。")
print("按下 Esc 结束自动注册脚本。")
print("")
print("")

i = 0
while i==0 do
	F9 = keyboard.getstate(120)			-- 取F9状态
	ADD = keyboard.getstate(187)			-- 取-状态
	SUB = keyboard.getstate(189)			-- 取+状态
	Esc = keyboard.getstate(27)			-- 取F11状态
	if Esc ~= 0 then				-- 按下F11退出脚本
		i = 1
	end
	if ADD ~= 0 then				-- 按下 + 增加注册ID
		regid = regid + 1
		print("注册ID增加1，当前ID为" .. regid)
		sleep(100)
	end
	if SUB ~= 0 then				-- 按下 - 减少注册ID
		regid = regid - 1
		print("注册ID减少1，当前ID为" .. regid)
		sleep(100)
	end
	if F9 == 0 then					-- F9没按下则继续等待
		sleep(100)
	else							-- 按下F9则开始模拟
		Regedit(regid)
	end
end