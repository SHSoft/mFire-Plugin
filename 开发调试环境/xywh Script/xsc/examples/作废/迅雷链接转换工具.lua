package.cpath = package.cpath..";"..exepath.."\\Lib\\lua\\?.dll;"

require("wx")

-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

-- character table string
local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

-- enconding
function enc(data)
     return ((data:gsub('.', function(x) 
         local r,b='',x:byte()
         for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
         return r;
     end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
         if (#x < 6) then return '' end
         local c=0
         for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
         return b:sub(c+1,c+1)
     end)..({ '', '==', '=' })[#data%3+1])
end

-- decoding
function dec(data)
     return (data:gsub('.', function(x)
         if (x == '=') then return '00' end
         local r,f='',(b:find(x)-1)
         for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
         return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)        
         if (#x ~= 8) then return '' end
         local c=0
         for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
         return string.char(c)
     end))
end

-- command line if not called as library
if (arg ~= nil) then
     local func = 'enc'
     for n,v in ipairs(arg) do
         if (n > 0) then
             if (v == "-h") then print "base64.lua [-e] [-d] text/data" break
             elseif (v == "-e") then func = 'enc'
             elseif (v == "-d") then func = 'dec'
             else print(_G[func](v)) end
         end
     end
else
     module('base64',package.seeall)
end 


function main()

ID_OK_BUTTON    = wx:wxNewId()
ID_CLOSE_BUTTON = wx:wxNewId()
ID_ADDBUTTON    = wx:wxNewId()
ID_CLEAR_BUTTON    = wx:wxNewId()

dialog = wx.wxDialog(wx.NULL, wx.wxID_ANY, "Ѹ������ת������",
                     wx.wxDefaultPosition, wx.wxDefaultSize)        

local mainSizer = wx.wxBoxSizer(wx.wxVERTICAL)

local nextSizer = wx.wxBoxSizer(wx.wxVERTICAL)

local buttonSizer = wx.wxBoxSizer( wx.wxHORIZONTAL )



multiText = wx.wxTextCtrl(dialog, -1, "", wx.wxDefaultPosition, wx.wxSize(350,80), wx.wxTE_MULTILINE)
multiText2 = wx.wxTextCtrl(dialog, -1, "", wx.wxDefaultPosition, wx.wxSize(350,80), wx.wxTE_MULTILINE)

nextSizer:Add(wx.wxStaticText( dialog, wx.wxID_ANY, "������Ѹ�����ӣ�"))
nextSizer:Add(multiText)

subButton = wx.wxButton( dialog, ID_OK_BUTTON, "ת��������")
closeButton = wx.wxButton( dialog, ID_CLOSE_BUTTON, "�ر�")
clearButton = wx.wxButton( dialog, ID_CLEAR_BUTTON, "���")
buttonSizer:Add( clearButton, 0, wx.wxALIGN_CENTER + wx.wxRIGHT, 5)

buttonSizer:Add( subButton, 0, wx.wxALIGN_CENTER + wx.wxRIGHT, 5)
buttonSizer:Add( closeButton, 0, wx.wxALIGN_CENTER + wx.wxEXPAND)
nextSizer:Add(wx.wxStaticLine(dialog, wx.wxID_ANY), 0, wx.wxEXPAND + wx.wxBOTTOM + wx.wxTOP, 14)
nextSizer:Add(wx.wxStaticText( dialog, wx.wxID_ANY, "���"))
nextSizer:Add(multiText2)
nextSizer:Add(wx.wxStaticLine(dialog, wx.wxID_ANY), 0, wx.wxEXPAND + wx.wxBOTTOM + wx.wxTOP, 14)
nextSizer:Add(buttonSizer, 0, wx.wxALIGN_RIGHT)


mainSizer:Add(nextSizer, 0, wx.wxALL, 14)

dialog:SetSizerAndFit(mainSizer)

dialog:Centre()
dialog:Show(true)

subButton:Connect(wx.wxEVT_COMMAND_BUTTON_CLICKED,
    function(event)
        _, _, str = string.find(multiText:GetValue(), "%s*thunder://(%S+)%s*")
        if str ~= nil then
            str = dec(str)
            multiText2:SetValue(string.sub(str, 3, string.len(str) - 3))
            text_data = wx.wxTextDataObject(multiText2:GetValue())
            local clipBoard = wx.wxClipboard.Get()
            if text_data and clipBoard and clipBoard:Open() then
                clipBoard.Get():SetData(text_data)
                clipBoard.Get():Flush()
                clipBoard.Get():Close()
            end


        else
            wx.wxMessageBox("��������ȷ��Ѹ�����ӣ�","����", wx.wxOK + wx.wxICON_ERROR)
        end
    end
)


clearButton:Connect(wx.wxEVT_COMMAND_BUTTON_CLICKED,
    function(event)
        multiText:SetValue("")
        multiText2:SetValue("")
    end
)


dialog:Connect(ID_CLOSE_BUTTON, wx.wxEVT_COMMAND_BUTTON_CLICKED,
    function(event) dialog:Destroy() end)

dialog:Connect(wx.wxEVT_CLOSE_WINDOW,
    function (event)
        dialog:Destroy()
        event:Skip()
    end)

    
end


main()

wx.wxGetApp():MainLoop()
