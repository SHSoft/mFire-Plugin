loadplugin("gfx.xsl",0)



rootpath = "C:\\Documents and Settings\\Administrator\\桌面\\RPG\\"
font = gfx.loadfont(rootpath.."res\\宋体.hzk")
--gfx.makefont(font,gfx.rgb(255,255,255))

Obj_Button = 1
Obj_Check = 2
Obj_Label = 3
Obj_Progres = 4
Obj_Track = 5
Obj_ListBox = 6



cons = 0
con = {}
mx = 0				-- 鼠标x坐标
my = 0				-- 鼠标y坐标
mw = 0				-- 鼠标滚轮值
mbtn = 0			-- 鼠标按键值


Panel = {id=0,x=0,y=0,w=100,h=100,objs=0,obj={},caption="FxGUI",img=0,imgback=0,visible=true,proc=0}
-- 面板控件[容器]
--		objs:子控件数量
--		obj:子控件对象列表
--		title:面板标题
--		backcall:面板消息回调
--		img:面板图标
--		imgback:面板背景图
--		visible:是否显示


Button	= {typ=Obj_Button,id=0,x=0,y=0,w=80,h=20,stk=0,stkbak=0,caption="",hotimg=0,downimg=0,defimg=0,visible=true,proc=0}
-- 按钮控件
--		stk:控件状态 [0=正常,1=鼠标划过,2=鼠标按下]
--		title:控件标题
--		defimg:正常状态显示的图片[创建时生成]
--		hotimg:鼠标划过显示的图片[创建时生成]
--		downimg:鼠标按下显示的图片[创建时生成]


Check	= {typ=Obj_Check,id=0,x=0,y=0,w=100,h=16,stk=0,stkbak=0,caption="",hotimg=0,downimg=0,defimg=0,hotchkimg=0,defchkimg=0,value=false,visible=true,proc=0}
-- 复选框
--		stk:控件状态 [0=正常,1=鼠标划过,2=鼠标按下,3=正常状态下选择,4=划过状态下选择]
--		title:复选框文字说明
--		value:选择值


Label	= {typ=Obj_Label,x=0,y=0,w=80,h=16,caption="",defimg=0,color=0,visible=true}
-- 文字标签
--		title:标签文字
--		defimg:显示内容[创建时生成]


Progres	= {typ=Obj_Progres,id=0,x=0,y=0,w=80,h=20,min=0,max=100,value=0,imgback=0,imgprog=0,visible=true}
-- 进度条
--		min:最小进度值
--		max:最大进度值
--		value:当前进度值
--		imgback:进度条背景图
--		imgprog:进度条进度图


Track	= {typ=Obj_Track,id=0,x=0,y=0,w=80,h=20,min=0,max=100,value=0,visible=true,proc=0}
-- 滑块指示器
--		min:最小值
--		max:最大值
--		value:当前值


ListBox	= {typ=Obj_ListBox,id=0,x=0,y=0,w=80,h=20,index,count,list={},visible=true,proc=0}
-- 列表框
--		index:当前选择
--		count:Item数量
--		list:列表项数据



function inRange(x,y,x1,y1,x2,y2,dostep)
	if dostep then
		if x>x1 and y>y1 and x<x1+x2 and y<y1+y2 then
			return true
		end
	else
		if x>x1 and y>y1 and x<x2 and y<y2 then
			return true
		end
	end
end



function Button.New(self,id,x,y,w,h,caption,event,callproc)
	o = {}
	setmetatable(o,self)
	self.__index = self
	o.id = id
	o.x = x
	o.y = y
	o.w = w
	o.h = h
	o.proc = callproc
	o.caption = caption
	o.defimg = gfx.imagecreate(o.w+1,o.h+1)
	o.hotimg = gfx.imagecreate(o.w+1,o.h+1)
	o.downimg = gfx.imagecreate(o.w+1,o.h+1)
	gfx.line(o.defimg,0,0,o.w,o.h,gfx.rgb(49,77,99),1)
	gfx.line(o.defimg,3,3,o.w-3,o.h-3,gfx.rgb(132,190,132),1)
	gfx.preset(o.defimg,0,0)
	gfx.preset(o.defimg,o.w,0)
	gfx.preset(o.defimg,0,o.h)
	gfx.preset(o.defimg,o.w,o.h)
	gfx.pset(o.defimg,3,3,gfx.rgb(49,77,99))
	gfx.pset(o.defimg,o.w-3,3,gfx.rgb(49,77,99))
	gfx.pset(o.defimg,3,o.h-3,gfx.rgb(49,77,99))
	gfx.pset(o.defimg,o.w-3,o.h-3,gfx.rgb(49,77,99))
	gfx.psettext(o.defimg,(o.w-(string.len(o.caption)*8))/2,(o.h-16)/2,o.caption,gfx.rgb(43,43,43))
	gfx.line(o.hotimg,0,0,o.w,o.h,gfx.rgb(28,44,57),1)
	gfx.line(o.hotimg,3,3,o.w-3,o.h-3,gfx.rgb(190,220,190),1)
	gfx.preset(o.hotimg,0,0)
	gfx.preset(o.hotimg,o.w,0)
	gfx.preset(o.hotimg,0,o.h)
	gfx.preset(o.hotimg,o.w,o.h)
	gfx.pset(o.hotimg,3,3,gfx.rgb(28,44,57))
	gfx.pset(o.hotimg,o.w-3,3,gfx.rgb(28,44,57))
	gfx.pset(o.hotimg,3,o.h-3,gfx.rgb(28,44,57))
	gfx.pset(o.hotimg,o.w-3,o.h-3,gfx.rgb(28,44,57))
	gfx.psettext(o.hotimg,(o.w-(string.len(o.caption)*8))/2,(o.h-16)/2,o.caption,gfx.rgb(28,44,57))
	gfx.line(o.downimg,0,0,o.w,o.h,gfx.rgb(49,77,99),1)
	gfx.line(o.downimg,3,3,o.w-3,o.h-3,gfx.rgb(28,44,57),1)
	gfx.preset(o.downimg,0,0)
	gfx.preset(o.downimg,o.w,0)
	gfx.preset(o.downimg,0,o.h)
	gfx.preset(o.downimg,o.w,o.h)
	gfx.pset(o.downimg,3,3,gfx.rgb(49,77,99))
	gfx.pset(o.downimg,o.w-3,3,gfx.rgb(49,77,99))
	gfx.pset(o.downimg,3,o.h-3,gfx.rgb(49,77,99))
	gfx.pset(o.downimg,o.w-3,o.h-3,gfx.rgb(49,77,99))
	gfx.psettext(o.downimg,(o.w-(string.len(o.caption)*8))/2,(o.h-16)/2,o.caption,gfx.rgb(255,255,255))
	if event then
		AddToEvent(o)
	end
	return o
end
function Button.Draw(self,img)
	if self.stk==1 then
		gfx.put(img,self.x,self.y,self.hotimg)
	elseif self.stk==2 then
		gfx.put(img,self.x,self.y,self.downimg)
	else
		gfx.put(img,self.x,self.y,self.defimg)
	end
end
function Button.Event(self)
	if inRange(mx,my,self.x,self.y,self.w,self.h,true) then
		if mbtn==1 then
			self.stk=2
			self.stkbak = 1
		else
			self.stk=1
			if type(self.proc)=="function" then
				if self.stkbak==1 then
					self.proc(self.id,0,0)
				end
			end
			self.stkbak = 0
		end
	else
		self.stk=0
		self.stkbak = 0
	end
end



function Label.New(self,x,y,w,color,caption)
	o = {}
	setmetatable(o,self)
	self.__index = self
	o.x = x
	o.y = y
	o.w = w
	o.color = color
	o.caption = caption
	o.defimg = gfx.imagecreate(o.w+1,16)
	gfx.psettext(o.defimg,0,0,o.caption,o.color)
	return o
end
function Label.Set(self,caption)
	self.caption = caption
	gfx.imagedestroy(self.defimg)
	self.defimg = gfx.imagecreate(self.w+1,16)
	gfx.psettext(self.defimg,0,0,self.caption,self.color)
end
function Label.Draw(self,img)
	gfx.put(img,self.x,self.y,self.defimg,0,0,self.w,16,PUT_TRANS)
end



function Check.New(self,id,x,y,caption,event,callproc)
	o = {}
	setmetatable(o,self)
	self.__index = self
	o.id = id
	o.x = x
	o.y = y
	o.proc = callproc
	o.caption = caption
	o.w = string.len(o.caption)*8 + 20
	o.h = 15
	o.defimg = gfx.imagecreate(o.w+1,o.h+1)
	o.hotimg = gfx.imagecreate(o.w+1,o.h+1)
	o.downimg = gfx.imagecreate(o.w+1,o.h+1)
	o.hotchkimg = gfx.imagecreate(o.w+1,o.h+1)
	o.defchkimg = gfx.imagecreate(o.w+1,o.h+1)
	gfx.line(o.defimg,0,0,15,15,gfx.rgb(49,77,99),1)
	gfx.line(o.defimg,2,2,13,13,gfx.rgb(132,190,132),1)
	gfx.preset(o.defimg,0,0)
	gfx.preset(o.defimg,15,0)
	gfx.preset(o.defimg,0,15)
	gfx.preset(o.defimg,15,15)
	gfx.psettext(o.defimg,20,0,o.caption,gfx.rgb(255,255,255))
	gfx.line(o.hotimg,0,0,15,15,gfx.rgb(28,44,57),1)
	gfx.line(o.hotimg,2,2,13,13,gfx.rgb(190,220,190),1)
	gfx.preset(o.hotimg,0,0)
	gfx.preset(o.hotimg,15,0)
	gfx.preset(o.hotimg,0,15)
	gfx.preset(o.hotimg,15,15)
	gfx.psettext(o.hotimg,20,0,o.caption,gfx.rgb(28,44,57))
	gfx.line(o.downimg,0,0,15,15,gfx.rgb(49,77,99),1)
	gfx.line(o.downimg,2,2,13,13,gfx.rgb(28,44,57),1)
	gfx.preset(o.downimg,0,0)
	gfx.preset(o.downimg,15,0)
	gfx.preset(o.downimg,0,15)
	gfx.preset(o.downimg,15,15)
	gfx.psettext(o.downimg,20,0,o.caption,gfx.rgb(28,44,57))
	gfx.line(o.defchkimg,0,0,15,15,gfx.rgb(49,77,99),1)
	gfx.line(o.defchkimg,2,2,13,13,gfx.rgb(28,44,57),1)
	gfx.line(o.defchkimg,4,4,11,11,gfx.rgb(180,180,180),1)
	gfx.preset(o.defchkimg,0,0)
	gfx.preset(o.defchkimg,15,0)
	gfx.preset(o.defchkimg,0,15)
	gfx.preset(o.defchkimg,15,15)
	gfx.pset(o.defchkimg,4,4,gfx.rgb(28,44,57))
	gfx.pset(o.defchkimg,11,4,gfx.rgb(28,44,57))
	gfx.pset(o.defchkimg,4,11,gfx.rgb(28,44,57))
	gfx.pset(o.defchkimg,11,11,gfx.rgb(28,44,57))
	gfx.psettext(o.defchkimg,20,0,o.caption,gfx.rgb(255,255,255))
	gfx.line(o.hotchkimg,0,0,15,15,gfx.rgb(28,44,57),1)
	gfx.line(o.hotchkimg,2,2,13,13,gfx.rgb(190,220,190),1)
	gfx.line(o.hotchkimg,4,4,11,11,gfx.rgb(49,77,99),1)
	gfx.preset(o.hotchkimg,0,0)
	gfx.preset(o.hotchkimg,15,0)
	gfx.preset(o.hotchkimg,0,15)
	gfx.preset(o.hotchkimg,15,15)
	gfx.pset(o.hotchkimg,4,4,gfx.rgb(190,220,190))
	gfx.pset(o.hotchkimg,11,4,gfx.rgb(190,220,190))
	gfx.pset(o.hotchkimg,4,11,gfx.rgb(190,220,190))
	gfx.pset(o.hotchkimg,11,11,gfx.rgb(190,220,190))
	gfx.psettext(o.hotchkimg,20,0,o.caption,gfx.rgb(28,44,57))
	if event then
		AddToEvent(o)
	end
	return o
end
function Check.Draw(self,img)
	if self.stk==1 then
		gfx.put(img,self.x,self.y,self.hotimg,0,0,self.w,16,PUT_TRANS)
	elseif self.stk==2 then
		gfx.put(img,self.x,self.y,self.downimg,0,0,self.w,16,PUT_TRANS)
	elseif self.stk==3 then
		gfx.put(img,self.x,self.y,self.defchkimg,0,0,self.w,16,PUT_TRANS)
	elseif self.stk==4 then
		gfx.put(img,self.x,self.y,self.hotchkimg,0,0,self.w,16,PUT_TRANS)
	else
		gfx.put(img,self.x,self.y,self.defimg,0,0,self.w,16,PUT_TRANS)
	end
end
function Check.Event(self)
	if inRange(mx,my,self.x,self.y,self.w,self.h,true) then
		if mbtn==1 then
			self.stk=2
			self.stkbak = 1
		else
			if self.value then
				self.stk=4
			else
				self.stk=1
			end
			if type(self.proc)=="function" then
				if self.stkbak==1 then
					self.value = not(self.value)
					self.proc(self.id,0,0)
				end
			end
			self.stkbak = 0
		end
	else
		if self.value then
			self.stk=3
		else
			self.stk=0
		end
		self.stkbak = 0
	end
end



function Progres.New(self,x,y,w,h)
	o = {}
	setmetatable(o,self)
	self.__index = self
	o.x = x
	o.y = y
	o.w = w
	o.h = h
	o.imgback = gfx.imagecreate(o.w+1,o.h+1)
	o.imgprog = gfx.imagecreate(o.w+1,o.h+1)
	gfx.line(o.imgback,0,0,o.w,o.h,gfx.rgb(49,77,99),1)
	gfx.line(o.imgback,2,2,o.w-2,o.h-2,gfx.rgb(132,190,132),1)
	gfx.preset(o.imgback,0,0)
	gfx.preset(o.imgback,o.w,0)
	gfx.preset(o.imgback,0,o.h)
	gfx.preset(o.imgback,o.w,o.h)
	gfx.pset(o.imgback,2,2)
	gfx.pset(o.imgback,o.w-2,2)
	gfx.pset(o.imgback,2,o.h-2)
	gfx.pset(o.imgback,o.w-2,o.h-2)
	return o
end
function Progres.set(self,value)
	self.value = value
	gfx.put(self.imgprog,0,0,self.imgback,0,0,self.w,o.h,PUT_TRANS)
	if self.value~=0 then
		gfx.line(self.imgprog,4,4,self.value/self.max*(self.w-4),self.h-4,gfx.rgb(28,44,57),1)
	end
end
function Progres.Draw(self,img)
	gfx.put(img,self.x,self.y,self.imgprog,0,0,self.w,self.h,PUT_TRANS)
end



function Panel.Refresh(self)
	
end
function Panel.Draw(self,toaddr)
	
end



function DoEvent()
	mx,my,mw,mbtn = gfx.getmouse()
	for k,v in ipairs(con) do
		if type(con[k].Event)=="function" then
			con[k]:Event()
		end
	end
end
function AddToEvent(conobj)
	cons=cons+1
	con[cons] = conobj
end







console.close()
gfx.screenres(120,216,16)
gfx.windowtitle("星月无痕鼠标自动点击器")

function dlqproc(id,wparam,lparam)
	if id==1 then
		isclick = true
	elseif id==2 then
		isclick = false
	elseif id==3 then
		waitms = tonumber(inputbox("请输入要设置的点击间隔 :","设置间隔",waitms))
		if not(waitms>0) then
			waitms = 1000
		end
	elseif id==4 then
		shell("explorer http://auto.wsdlq.net")
	elseif id==5 then
		shell("explorer http://auto.wsdlq.net/bbs")
	elseif id==6 then
		i = 1
	end
end



a = Button:New(1,10,10,100,24,"开始点击",true,dlqproc)
b = Button:New(2,10,44,100,24,"停止点击",true,dlqproc)
c = Button:New(3,10,78,100,24,"设置间隔",true,dlqproc)
d = Button:New(4,10,112,100,24,"访问官网",true,dlqproc)
e = Button:New(5,10,146,100,24,"访问论坛",true,dlqproc)
f = Button:New(6,10,180,100,24,"退出程序",true,dlqproc)



i = 0
waitms = 1000
clickms = 0
isclick = false
repeat
	gfx.screensync()						-- 开启垂直同步
	gfx.screenlock()
	gfx.cls()
	DoEvent()
	for k,v in ipairs(con) do
		con[k]:Draw(0)
	end
	gfx.screenunlock()
	if isclick then
		if clickms>waitms then
			mouse.click(0)			-- 点击鼠标
		end
	end
	clickms = clickms+50
	sleep(50)
until i==1
gfx.screen(0)
os.exit()