#Ifndef xywh_library_xsc
	#Include Once "Windows.bi"
	#Include Once "crt/stddef.bi"
	#Include Once "crt/stdarg.bi"
	#Include Once "crt/stdio.bi"
	#Define xywh_library_xsc
	#Inclib "xsc"
	
	'----------------------------------------------------------------------------------
	'									xywh Script SDK 接口API
	'----------------------------------------------------------------------------------
	Declare Function xsc_new() As Any Ptr														' 创建 LUA 虚拟机 , 并注册 xsc 附加库
	Declare Sub xsc_free(L As Any Ptr)															' 释放 LUA 虚拟机
	Declare Function xsc_exec(L As Any Ptr, istr As Integer, txt As ZString Ptr) As Integer		' 运行 LUA 代码 [istr=TRUE运行字符串,否则运行文件]
	Declare Function xsc_port(Index As Integer, Param As Any Ptr) As Any Ptr					' 信息端口 [可以获取LUA全部函数]
	Declare Function xsc_loadplugin(L As Any Ptr, hdr As Any Ptr, param As Integer) As Integer	' 载入插件
	Declare Function xsc_freeplugin(L As Any Ptr, hdr As Any Ptr, param As Integer) As Integer	' 释放插件
	
	
	
	'----------------------------------------------------------------------------------
	'								xywh Script SDK 导出API
	'----------------------------------------------------------------------------------
	Dim Shared MemoryLoadLibrary As Function(addr As Any Ptr) As HANDLE
	Dim Shared MemoryFreeLibrary As Sub(hdr As HANDLE)
	Dim Shared MemoryGetProcAddress As Function(hdr As HANDLE,proc As LPCSTR) As Any Ptr
	Dim Shared xywh_Run As Function(Path As ZString Ptr,WinShow As Integer) As Integer
	Dim Shared xywh_Shell As Function(Path As ZString Ptr,WinShow As Integer) As Integer
	Dim Shared xywh_Chain As Function(Path As ZString Ptr,WinShow As Integer) As Integer
	
	
	
	'----------------------------------------------------------------------------------
	'								xywh Script 内部数据表
	'----------------------------------------------------------------------------------
	Dim Shared xscVer As Integer					' 宿主程序版本
	Dim Shared PortVer As Integer					' 接口版本
	Dim Shared LuaVer As Integer					' LUA解释器版本
	
	
	
	'----------------------------------------------------------------------------------
	'									#Define 定义
	'----------------------------------------------------------------------------------
	#Define LUA_VERSION_NUM				503
	#Define LUA_MULTRET					(-1)
	#Define LUAI_MAXSTACK				1000000
	#Define LUA_REGISTRYINDEX			(-LUAI_MAXSTACK - 1000)
	#Define lua_upvalueindex(i)			(LUA_REGISTRYINDEX - (i))
	#Define LUA_OK						0
	#Define LUA_YIELD					1
	#Define LUA_ERRRUN					2
	#Define LUA_ERRSYNTAX				3
	#Define LUA_ERRMEM					4
	#Define LUA_ERRGCMM					5
	#Define LUA_ERRERR					6
	#Define LUA_ERRFILE					(LUA_ERRERR+1)
	#Define LUA_TNONE					(-1)
	#Define LUA_TNIL					0
	#Define LUA_TBOOLEAN				1
	#Define LUA_TLIGHTUSERDATA			2
	#Define LUA_TNUMBER					3
	#Define LUA_TSTRING					4
	#Define LUA_TTABLE					5
	#Define LUA_TFUNCTION				6
	#Define LUA_TUSERDATA				7
	#Define LUA_TTHREAD					8
	#Define LUA_NUMTAGS					9
	#Define LUA_MINSTACK				20
	#Define LUA_RIDX_MAINTHREAD			1
	#Define LUA_RIDX_GLOBALS			2
	#Define LUA_RIDX_LAST				LUA_RIDX_GLOBALS
	#Define LUA_OPADD					0	/' ORDER TM, ORDER OP '/
	#Define LUA_OPSUB					1
	#Define LUA_OPMUL					2
	#Define LUA_OPMOD					3
	#Define LUA_OPPOW					4
	#Define LUA_OPDIV					5
	#Define LUA_OPIDIV					6
	#Define LUA_OPBAND					7
	#Define LUA_OPBOR					8
	#Define LUA_OPBXOR					9
	#Define LUA_OPSHL					10
	#Define LUA_OPSHR					11
	#Define LUA_OPUNM					12
	#Define LUA_OPBNOT					13
	#Define LUA_OPEQ					0
	#Define LUA_OPLT					1
	#Define LUA_OPLE					2
	#Define LUA_GCSTOP					0
	#Define LUA_GCRESTART				1
	#Define LUA_GCCOLLECT				2
	#Define LUA_GCCOUNT					3
	#Define LUA_GCCOUNTB				4
	#Define LUA_GCSTEP					5
	#Define LUA_GCSETPAUSE				6
	#Define LUA_GCSETSTEPMUL			7
	#Define LUA_GCISRUNNING				9
	#Define LUA_HOOKCALL				0
	#Define LUA_HOOKRET					1
	#Define LUA_HOOKLINE				2
	#Define LUA_HOOKCOUNT				3
	#Define LUA_HOOKTAILCALL			4
	#Define LUA_MASKCALL				(1 Shl LUA_HOOKCALL)
	#Define LUA_MASKRET					(1 Shl LUA_HOOKRET)
	#Define LUA_MASKLINE				(1 Shl LUA_HOOKLINE)
	#Define LUA_MASKCOUNT				(1 Shl LUA_HOOKCOUNT)
	' lauxlib 定义
	#Define LUAL_NUMSIZES				(sizeof(lua_Integer)*16 + sizeof(lua_Number))
	#Define LUA_NOREF					(-2)
	#Define LUA_REFNIL					(-1)
	#Define LUA_IDSIZE					60
	#Define LUAL_BUFFERSIZE				8192
	
	
	
	'----------------------------------------------------------------------------------
	'								xywh Script 追加宏定义
	'----------------------------------------------------------------------------------
	#Define lua_regclassfunc(L,n,f) lua_pushstring(L,n):lua_pushcfunction(L,f):lua_settable(L,-3)
	#Define lua_setclassstr(L,n,s) lua_pushstring(L,n):lua_pushstring(L,s):lua_settable(L,-3)
	#Define lua_setclassint(L,n,i) lua_pushstring(L,n):lua_pushnumber(L,i):lua_settable(L,-3)
	#Define lua_setglobalstr(L,n,s) lua_pushstring(L,s):lua_setglobal(L,(n))
	#Define lua_setglobalint(L,n,i) lua_pushnumber(L,i):lua_setglobal(L,(n))
	#Define lua_unregister(L,n) lua_pushnil(L): lua_setglobal(L, (n))
	
	
	
	'-----------------------------------------------------------------------------
	'								类型与结构体定义
	'-----------------------------------------------------------------------------
	Type lua_State As Any Ptr
	Type lua_Number As Double
	Type lua_Integer As LongInt
	Type lua_Unsigned As ULongInt
	Type lua_KContext As Any Ptr
	Type lua_CFunction As Function Cdecl(L As Any Ptr) As Integer
	Type lua_KFunction As Function Cdecl(L As Any Ptr, status As Integer, ctx As lua_KContext) As Integer
	Type lua_Reader As Function Cdecl(L As Any Ptr, ud As Any Ptr, sz As size_t Ptr) As Any Ptr
	Type lua_Writer As Function Cdecl(L As Any Ptr, p As Any Ptr, sz As size_t, ud As Any Ptr) As Integer
	Type lua_Alloc As Function Cdecl(ud As Any Ptr, p As Any Ptr, osz As size_t, nsz As size_t) As Any Ptr
	Type lua_Hook As Function Cdecl(L As Any Ptr,ar As Any Ptr) As Any Ptr
	Type lua_Debug
		event As Integer
		name As ZString Ptr
		namewhat As ZString Ptr
		what As ZString Ptr
		source As ZString Ptr
		currentline As Integer
		linedefined As Integer
		lastlinedefined As Integer
		nups As UByte
		nparams As UByte
		isvararg As Byte
		istailcall As Byte
		short_src As ZString * LUA_IDSIZE
		i_ci As Any Ptr
	End Type
	' lauxlib 类型
	Type luaL_Reg
		name As ZString Ptr
		func As lua_CFunction
	End Type
	Type luaL_Buffer
		b As Byte Ptr
		size As size_t
		n As size_t
		L As lua_State Ptr
		initb(0 to LUAL_BUFFERSIZE-1) As Byte
	End Type
	Type luaL_Stream
		f As FILE Ptr
		closef As lua_CFunction
	End Type
	
	
	
	'-----------------------------------------------------------------------------
	'								LUA导出函数定义
	'-----------------------------------------------------------------------------
	Extern "c"
		' lua 函数
		Dim Shared lua_newstate As Function(f As lua_Alloc, ud As Any Ptr) As lua_State Ptr
		Dim Shared lua_close As Sub(L As lua_State Ptr)
		Dim Shared lua_newthread As Function(L As lua_State Ptr) As lua_State Ptr
		Dim Shared lua_atpanic As Function(L As lua_State Ptr, panicf As lua_CFunction) As lua_CFunction
		Dim Shared lua_version As Function(L As lua_State Ptr) As lua_Number Ptr
		Dim Shared lua_absindex As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_gettop As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_settop As Sub(L As lua_State Ptr, idx As Integer)
		Dim Shared lua_pushvalue As Sub(L As lua_State Ptr, idx As Integer)
		Dim Shared lua_rotate As Sub(L As lua_State Ptr, idx As Integer, n As Integer)
		Dim Shared lua_copy As Sub(L As lua_State Ptr, fromidx As Integer, toidx As Integer)
		Dim Shared lua_checkstack As Function(L As lua_State Ptr, n As Integer) As Integer
		Dim Shared lua_xmove As Sub(f As lua_State Ptr, t As lua_State Ptr, n As Integer)
		Dim Shared lua_isnumber As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_isstring As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_iscfunction As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_isinteger As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_isuserdata As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_type As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_typename As Function(L As lua_State Ptr, tp As Integer) As ZString Ptr
		Dim Shared lua_tonumberx As Function(L As lua_State Ptr, idx As Integer, isnum As Integer Ptr) As lua_Number
		Dim Shared lua_tointegerx As Function(L As lua_State Ptr, idx As Integer, isnum As Integer Ptr) As lua_Integer
		Dim Shared lua_toboolean As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_tolstring As Function(L As lua_State Ptr, idx As Integer, l As size_t Ptr) As ZString Ptr
		Dim Shared lua_rawlen As Function(L As lua_State Ptr, idx As Integer) As size_t
		Dim Shared lua_tocfunction As Function(L As lua_State Ptr, idx As Integer) As lua_CFunction
		Dim Shared lua_touserdata As Function(L As lua_State Ptr, idx As Integer) As Any Ptr
		Dim Shared lua_tothread As Function(L As lua_State Ptr, idx As Integer) As lua_State Ptr
		Dim Shared lua_topointer As Function(L As lua_State Ptr, idx As Integer) As Any Ptr
		Dim Shared lua_arith As Function(L As lua_State Ptr, op As Integer) As Integer
		Dim Shared lua_rawequal As Function(L As lua_State Ptr, idx1 As Integer, idx2 As Integer) As Integer
		Dim Shared lua_compare As Function(L As lua_State Ptr, idx1 As Integer, idx2 As Integer, op As Integer) As Integer
		Dim Shared lua_pushnil As Sub(L As lua_State Ptr)
		Dim Shared lua_pushnumber As Sub(L As lua_State Ptr, n As lua_Number)
		Dim Shared lua_pushinteger As Sub(L As lua_State Ptr, n As lua_Integer)
		Dim Shared lua_pushlstring As Function(L As lua_State Ptr, s As ZString Ptr, l As size_t) As ZString Ptr
		Dim Shared lua_pushstring As Function(L As lua_State Ptr, s As ZString Ptr) As ZString Ptr
		Dim Shared lua_pushvfstring As Function(L As lua_State Ptr, fmt As ZString Ptr, argp As va_list) As ZString Ptr
		Dim Shared lua_pushfstring As Function(L As lua_State Ptr, fmt As ZString Ptr, ...) As ZString Ptr
		Dim Shared lua_pushcclosure As Sub(L As lua_State Ptr, fn As lua_CFunction, n As Integer)
		Dim Shared lua_pushboolean As Sub(L As lua_State Ptr, b As Integer)
		Dim Shared lua_pushlightuserdata As Sub(L As lua_State Ptr, p As Any Ptr)
		Dim Shared lua_pushthread As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_getglobal As Function(L As lua_State Ptr, vname As ZString Ptr) As Integer
		Dim Shared lua_gettable As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_getfield As Function(L As lua_State Ptr, idx As Integer, k As ZString Ptr) As Integer
		Dim Shared lua_geti As Function(L As lua_State Ptr, idx As Integer, n As lua_Integer) As Integer
		Dim Shared lua_rawget As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_rawgeti As Function(L As lua_State Ptr, idx As Integer, n As lua_Integer) As Integer
		Dim Shared lua_rawgetp As Function(L As lua_State Ptr, idx As Integer, p As ZString Ptr) As Integer
		Dim Shared lua_createtable As Sub(L As lua_State Ptr, narr As Integer, nrec As Integer)
		Dim Shared lua_newuserdata As Function(L As lua_State Ptr, sz As size_t) As Any Ptr
		Dim Shared lua_getmetatable As Function(L As lua_State Ptr, objindex As Integer) As Integer
		Dim Shared lua_getuservalue As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_setglobal As Sub(L As lua_State Ptr, vname As ZString Ptr)
		Dim Shared lua_settable As Sub(L As lua_State Ptr, idx As Integer)
		Dim Shared lua_setfield As Sub(L As lua_State Ptr, idx As Integer, k As ZString Ptr)
		Dim Shared lua_seti As Sub(L As lua_State Ptr, idx As Integer, n As lua_Integer)
		Dim Shared lua_rawset As Sub(L As lua_State Ptr, idx As Integer)
		Dim Shared lua_rawseti As Sub(L As lua_State Ptr, idx As Integer, n As lua_Integer)
		Dim Shared lua_rawsetp As Sub(L As lua_State Ptr, idx As Integer, p As ZString Ptr)
		Dim Shared lua_setmetatable As Function(L As lua_State Ptr, objindex As Integer) As Integer
		Dim Shared lua_setuservalue As Sub(L As lua_State Ptr, idx As Integer)
		Dim Shared lua_callk As Sub(L As lua_State Ptr, nargs As Integer, nresults As Integer, ctx As lua_KContext, k As lua_KFunction)
		Dim Shared lua_pcallk As Function(L As lua_State Ptr, nargs As Integer, nresults As Integer, errfunc As Integer, ctx As lua_KContext, k As lua_KFunction) As Integer
		Dim Shared lua_load As Function(L As lua_State Ptr, reader As lua_Reader, dt As Any Ptr, chunkname As ZString Ptr, mode As ZString Ptr) As Integer
		Dim Shared lua_dump As Function(L As lua_State Ptr, writer As lua_Writer, dat As Any Ptr, strip As Integer) As Integer
		Dim Shared lua_yieldk As Function(L As lua_State Ptr, nresults As Integer, ctx As lua_KContext, k As lua_KFunction) As Integer
		Dim Shared lua_resume As Function(L As lua_State Ptr, from As lua_State Ptr, narg As Integer) As Integer
		Dim Shared lua_status As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_isyieldable As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_gc As Function(L As lua_State Ptr, what As Integer, dat As Integer) As Integer
		Dim Shared lua_error As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_next As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared lua_concat As Sub(L As lua_State Ptr, n As Integer)
		Dim Shared lua_len As Function(L As lua_State Ptr, idx As Integer) As Any Ptr
		Dim Shared lua_stringtonumber As Function(L As lua_State Ptr, s As ZString Ptr) As size_t
		Dim Shared lua_getallocf As Function(L As lua_State Ptr, ud As Any Ptr Ptr) As lua_Alloc
		Dim Shared lua_setallocf As Sub(L As lua_State Ptr, f As lua_Alloc, ud As Any Ptr)
		Dim Shared lua_getstack As Function(L As lua_State Ptr, level As Integer, ar As lua_Debug Ptr) As Integer
		Dim Shared lua_getinfo As Function(L As lua_State Ptr, what As ZString Ptr, ar As lua_Debug Ptr) As Integer
		Dim Shared lua_getlocal As Function(L As lua_State Ptr, ar As lua_Debug Ptr, n As Integer) As ZString Ptr
		Dim Shared lua_setlocal As Function(L As lua_State Ptr, ar As lua_Debug Ptr, n As Integer) As ZString Ptr
		Dim Shared lua_getupvalue As Function(L As lua_State Ptr, funcindex As Integer, n As Integer) As ZString Ptr
		Dim Shared lua_setupvalue As Function(L As lua_State Ptr, funcindex As Integer, n As Integer) As ZString Ptr
		Dim Shared lua_upvalueid As Function(L As lua_State Ptr, fidx As Integer, n As Integer) As Any Ptr
		Dim Shared lua_upvaluejoin As Sub(L As lua_State Ptr, fidx1 As Integer, n1 As Integer, fidx2 As Integer, n2 As Integer)
		Dim Shared lua_sethook As Function(L As lua_State Ptr, func As lua_Hook, mask As Integer, count As Integer) As Integer
		Dim Shared lua_gethook As Function(L As lua_State Ptr) As lua_Hook
		Dim Shared lua_gethookmask As Function(L As lua_State Ptr) As Integer
		Dim Shared lua_gethookcount As Function(L As lua_State Ptr) As Integer
		' lualib 函数
		Dim Shared luaopen_base As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_coroutine As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_table As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_io As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_os As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_string As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_utf8 As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_bit32 As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_math As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_debug As Function(L As lua_State Ptr) As Integer
		Dim Shared luaopen_package As Function(L As lua_State Ptr) As Integer
		Dim Shared luaL_openlibs As Sub(L As lua_State Ptr)
		' lauxlib 函数
		Dim Shared luaL_checkversion_ As Sub(L As lua_State Ptr, ver As lua_Number, sz As size_t)
		Dim Shared luaL_getmetafield As Function(L As lua_State Ptr, obj As Integer, e As ZString Ptr) As Integer
		Dim Shared luaL_callmeta As Function(L As lua_State Ptr, obj As Integer, e As ZString Ptr) As Integer
		Dim Shared luaL_tolstring As Function(L As lua_State Ptr, idx As Integer, len As size_t Ptr) As ZString Ptr
		Dim Shared luaL_argerror As Function(L As lua_State Ptr, arg As Integer, extramsg As ZString Ptr) As Integer
		Dim Shared luaL_checklstring As Function(L As lua_State Ptr, arg As Integer, l As size_t Ptr) As ZString Ptr
		Dim Shared luaL_optlstring As Function(L As lua_State Ptr, arg As Integer, def As ZString Ptr, l As size_t Ptr) As ZString Ptr
		Dim Shared luaL_checknumber As Function(L As lua_State Ptr, arg As Integer) As lua_Number
		Dim Shared luaL_optnumber As Function(L As lua_State Ptr, arg As Integer, def As lua_Number) As lua_Number
		Dim Shared luaL_checkinteger As Function(L As lua_State Ptr, arg As Integer) As lua_Integer
		Dim Shared luaL_optinteger As Function(L As lua_State Ptr, arg As Integer, def As lua_Integer) As lua_Integer
		Dim Shared luaL_checkstack As Sub(L As lua_State Ptr, sz As Integer, msg As ZString Ptr)
		Dim Shared luaL_checktype As Sub(L As lua_State Ptr, arg As Integer, t As Integer)
		Dim Shared luaL_checkany As Sub(L As lua_State Ptr, arg As Integer)
		Dim Shared luaL_newmetatable As Function(L As lua_State Ptr, tname As ZString Ptr) As Integer
		Dim Shared luaL_setmetatable As Sub(L As lua_State Ptr, tname As ZString Ptr)
		Dim Shared luaL_testudata As Function(L As lua_State Ptr, ud As Integer, tname As ZString Ptr) As Any Ptr
		Dim Shared luaL_checkudata As Function(L As lua_State Ptr, ud As Integer, tname As ZString Ptr) As Any Ptr
		Dim Shared luaL_where As Sub(L As lua_State Ptr, lvl As Integer)
		Dim Shared luaL_error As Function(L As lua_State Ptr, fmt As ZString Ptr, ...) As Integer
		Dim Shared luaL_checkoption As Function(L As lua_State Ptr, arg As Integer, def As ZString Ptr, lst As byte Ptr Ptr) As Integer
		Dim Shared luaL_fileresult As Function(L As lua_State Ptr, stat As Integer, fname As ZString Ptr) As Integer
		Dim Shared luaL_execresult As Function(L As lua_State Ptr, stat As Integer) As Integer
		Dim Shared luaL_ref As Function(L As lua_State Ptr, t As Integer) As Integer
		Dim Shared luaL_unref As Sub(L As lua_State Ptr, t As Integer, ref As Integer)
		Dim Shared luaL_loadfilex As Function(L As lua_State Ptr, filename As ZString Ptr, mode As ZString Ptr) As Integer
		Dim Shared luaL_loadbufferx As Function(L As lua_State Ptr, buff As ZString Ptr, sz As size_t, fname As ZString Ptr, mode As ZString Ptr) As Integer
		Dim Shared luaL_loadstring As Function(L As lua_State Ptr, s As ZString Ptr) As Integer
		Dim Shared luaL_newstate As Function() As lua_State Ptr
		Dim Shared luaL_len As Function(L As lua_State Ptr, idx As Integer) As Integer
		Dim Shared luaL_gsub As Function(L As lua_State Ptr, s As ZString Ptr, p As ZString Ptr, r As ZString Ptr) As ZString Ptr
		Dim Shared luaL_setfuncs As Sub(L As lua_State Ptr, lr As luaL_Reg Ptr, nup As Integer)
		Dim Shared luaL_getsubtable As Function(L As lua_State Ptr, idx As Integer, fname As ZString Ptr) As Integer
		Dim Shared luaL_traceback As Sub(L As lua_State Ptr, L1 As lua_State Ptr, msg As ZString Ptr, level As Integer)
		Dim Shared luaL_requiref As Function(L As lua_State Ptr, modname As ZString Ptr, openf As lua_CFunction, glb As Integer) As Any Ptr
		Dim Shared luaL_buffinit As Sub(L As lua_State Ptr, B As luaL_Buffer Ptr)
		Dim Shared luaL_prepbuffsize As Function(B As luaL_Buffer Ptr, sz As size_t) As ZString Ptr
		Dim Shared luaL_addlstring As Sub(B As luaL_Buffer Ptr, s As ZString Ptr, l As size_t)
		Dim Shared luaL_addstring As Sub(B As luaL_Buffer Ptr, s As ZString Ptr)
		Dim Shared luaL_addvalue As Sub(B As luaL_Buffer Ptr)
		Dim Shared luaL_pushresult As Sub(B As luaL_Buffer Ptr)
		Dim Shared luaL_pushresultsize As Sub(B As luaL_Buffer Ptr, sz As size_t)
		Dim Shared luaL_buffinitsize As Function(L As lua_State Ptr, B As luaL_Buffer Ptr, sz As size_t) As ZString Ptr
	End Extern
	
	
	
	'-----------------------------------------------------------------------------
	'								LUA功能简化宏定义
	'-----------------------------------------------------------------------------
	#Define lua_call(L,n,r)				lua_callk(L, (n), (r), 0, NULL)
	#Define lua_pcall(L,n,r,f)			lua_pcallk(L, (n), (r), (f), 0, NULL)
	#Define lua_yield_(L,n)				lua_yieldk(L, (n), 0, NULL)
	#Define lua_getextraspace(L)		(Cast(Any Ptr, (L) - LUA_EXTRASPACE))
	#Define lua_tonumber(L,i)			lua_tonumberx(L,(i),NULL)
	#Define lua_tointeger(L,i)			lua_tointegerx(L,(i),NULL)
	#Define lua_pop(L,n)				lua_settop(L, -(n)-1)
	#Define lua_newtable(L)				lua_createtable(L, 0, 0)
	#Define lua_register(L,n,f)			lua_pushcfunction(L, (f)) : lua_setglobal(L, (n))
	#Define lua_pushcfunction(L,f)		lua_pushcclosure(L, (f), 0)
	#Define lua_isfunction(L,n)			(lua_type(L, (n)) = LUA_TFUNCTION)
	#Define lua_istable(L,n)			(lua_type(L, (n)) = LUA_TTABLE)
	#Define lua_islightuserdata(L,n)	(lua_type(L, (n)) = LUA_TLIGHTUSERDATA)
	#Define lua_isnil(L,n)				(lua_type(L, (n)) = LUA_TNIL)
	#Define lua_isboolean(L,n)			(lua_type(L, (n)) = LUA_TBOOLEAN)
	#Define lua_isthread(L,n)			(lua_type(L, (n)) = LUA_TTHREAD)
	#Define lua_isnone(L,n)				(lua_type(L, (n)) = LUA_TNONE)
	#Define lua_isnoneornil(L, n)		(lua_type(L, (n)) <= 0)
	#Define lua_pushliteral(L, s)		lua_pushlstring(L, "" s, (sizeof(s)/sizeof(char))-1)
	#Define lua_pushglobaltable(L)		lua_rawgeti(L, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS)
	#Define lua_tostring(L,i)			lua_tolstring(L, (i), NULL)
	#Define lua_insert(L,idx)			lua_rotate(L, (idx), 1)
	#Define lua_remove(L,idx)			(lua_rotate(L, (idx), -1), lua_pop(L, 1))
	#Define lua_replace(L,idx)			(lua_copy(L, -1, (idx)), lua_pop(L, 1))
	' lauxlib 定义
	#define luaL_checkversion(L)		luaL_checkversion_(L, LUA_VERSION_NUM, LUAL_NUMSIZES)
	#Define luaL_loadfile(L,f)			luaL_loadfilex(L, f, NULL)
	#Define luaL_newlibtable(L,arr)		lua_createtable(L, 0, (UBound(arr) - LBound(arr)) + 1)
	#Define luaL_newlib(L,arr)			(luaL_checkversion(L) : luaL_newlibtable(L,arr) : luaL_setfuncs(L,arr,0))
	#Define luaL_argcheck(L, c,arg,ext)	((c) Or luaL_argerror(L, (arg), (ext)))
	#Define luaL_checkstring(L,n)		(luaL_checklstring(L, (n), NULL))
	#Define luaL_optstring(L,n,d)		(luaL_optlstring(L, (n), (d), NULL))
	#Define luaL_typename(L,i)			lua_typename(L, lua_type(L,(i)))
	#Define luaL_dofile(L, fn)			(luaL_loadfile(L, fn) OrElse lua_pcall(L, 0, LUA_MULTRET, 0))
	#Define luaL_dostring(L, s)			(luaL_loadstring(L, s) OrElse lua_pcall(L, 0, LUA_MULTRET, 0))
	#Define luaL_getmetatable(L,n)		(lua_getfield(L, LUA_REGISTRYINDEX, (n)))
	#Define luaL_opt(L,f,n,d)			IIf(lua_isnoneornil(L,(n)), d, f(L,(n)))
	#Define luaL_loadbuffer(L,s,sz,n)	luaL_loadbufferx(L,s,sz,n,NULL)
	#Define luaL_addchar(B,c)			(If (B)->n >= (B)->size Then luaL_prepbuffsize((B), 1) : (B)->b((B)->n+2) = (c))
	#Define luaL_addsize(B,s)			((B)->n += (s))
	#Define luaL_prepbuffer(B)			luaL_prepbuffsize(B, LUAL_BUFFERSIZE)
	
	
	
	'--------------------------------------------------------------------
	'												xywh Script SDK 初始化
	'--------------------------------------------------------------------
	Sub InitXSC()
		' ID : 1 to 100 内部数据
		xscVer =				Cast(Integer,xsc_port(1, NULL))
		PortVer =				Cast(Integer,xsc_port(2, NULL))
		LuaVer =				Cast(Integer,xsc_port(3, NULL))
		' ID : 201 to 300 其他SDK函数
		MemoryLoadLibrary =		xsc_port(201, NULL)
		MemoryFreeLibrary =		xsc_port(202, NULL)
		MemoryGetProcAddress =	xsc_port(203, NULL)
		xywh_Run =				xsc_port(211, NULL)
		xywh_Shell =			xsc_port(212, NULL)
		xywh_Chain =			xsc_port(213, NULL)
		' ID : 1001 to 2000 LUA函数 [核心函数]
		lua_newstate =			xsc_port(1001, NULL)
		lua_close =				xsc_port(1002, NULL)
		lua_newthread =			xsc_port(1003, NULL)
		lua_atpanic =			xsc_port(1004, NULL)
		lua_version =			xsc_port(1005, NULL)
		lua_absindex =			xsc_port(1006, NULL)
		lua_gettop =			xsc_port(1007, NULL)
		lua_settop =			xsc_port(1008, NULL)
		lua_pushvalue =			xsc_port(1009, NULL)
		lua_rotate =			xsc_port(1010, NULL)
		lua_copy =				xsc_port(1011, NULL)
		lua_checkstack =		xsc_port(1012, NULL)
		lua_xmove =				xsc_port(1013, NULL)
		lua_isnumber =			xsc_port(1014, NULL)
		lua_isstring =			xsc_port(1091, NULL)
		lua_iscfunction =		xsc_port(1015, NULL)
		lua_isinteger =			xsc_port(1016, NULL)
		lua_isuserdata =		xsc_port(1017, NULL)
		lua_type =				xsc_port(1018, NULL)
		lua_typename =			xsc_port(1019, NULL)
		lua_tonumberx =			xsc_port(1020, NULL)
		lua_tointegerx =		xsc_port(1021, NULL)
		lua_toboolean =			xsc_port(1022, NULL)
		lua_tolstring =			xsc_port(1023, NULL)
		lua_rawlen =			xsc_port(1024, NULL)
		lua_tocfunction =		xsc_port(1025, NULL)
		lua_touserdata =		xsc_port(1026, NULL)
		lua_tothread =			xsc_port(1027, NULL)
		lua_topointer =			xsc_port(1028, NULL)
		lua_arith =				xsc_port(1029, NULL)
		lua_rawequal =			xsc_port(1030, NULL)
		lua_compare =			xsc_port(1031, NULL)
		lua_pushnil =			xsc_port(1032, NULL)
		lua_pushnumber =		xsc_port(1033, NULL)
		lua_pushinteger =		xsc_port(1034, NULL)
		lua_pushlstring =		xsc_port(1035, NULL)
		lua_pushstring =		xsc_port(1036, NULL)
		lua_pushvfstring =		xsc_port(1037, NULL)
		lua_pushfstring =		xsc_port(1038, NULL)
		lua_pushcclosure =		xsc_port(1039, NULL)
		lua_pushboolean =		xsc_port(1040, NULL)
		lua_pushlightuserdata =	xsc_port(1041, NULL)
		lua_pushthread =		xsc_port(1042, NULL)
		lua_getglobal =			xsc_port(1043, NULL)
		lua_gettable =			xsc_port(1044, NULL)
		lua_getfield =			xsc_port(1045, NULL)
		lua_geti =				xsc_port(1046, NULL)
		lua_rawget =			xsc_port(1047, NULL)
		lua_rawgeti =			xsc_port(1048, NULL)
		lua_rawgetp =			xsc_port(1049, NULL)
		lua_createtable =		xsc_port(1050, NULL)
		lua_newuserdata =		xsc_port(1051, NULL)
		lua_getmetatable =		xsc_port(1052, NULL)
		lua_getuservalue =		xsc_port(1053, NULL)
		lua_setglobal =			xsc_port(1054, NULL)
		lua_settable =			xsc_port(1055, NULL)
		lua_setfield =			xsc_port(1056, NULL)
		lua_seti =				xsc_port(1057, NULL)
		lua_rawset =			xsc_port(1058, NULL)
		lua_rawseti =			xsc_port(1059, NULL)
		lua_rawsetp =			xsc_port(1060, NULL)
		lua_setmetatable =		xsc_port(1061, NULL)
		lua_setuservalue =		xsc_port(1062, NULL)
		lua_callk =				xsc_port(1063, NULL)
		lua_pcallk =			xsc_port(1064, NULL)
		lua_load =				xsc_port(1065, NULL)
		lua_dump =				xsc_port(1066, NULL)
		lua_yieldk =			xsc_port(1067, NULL)
		lua_resume =			xsc_port(1068, NULL)
		lua_status =			xsc_port(1069, NULL)
		lua_isyieldable =		xsc_port(1070, NULL)
		lua_gc =				xsc_port(1071, NULL)
		lua_error =				xsc_port(1072, NULL)
		lua_next =				xsc_port(1073, NULL)
		lua_concat =			xsc_port(1074, NULL)
		lua_len =				xsc_port(1075, NULL)
		lua_stringtonumber =	xsc_port(1076, NULL)
		lua_getallocf =			xsc_port(1077, NULL)
		lua_setallocf =			xsc_port(1078, NULL)
		lua_getstack =			xsc_port(1079, NULL)
		lua_getinfo =			xsc_port(1080, NULL)
		lua_getlocal =			xsc_port(1081, NULL)
		lua_setlocal =			xsc_port(1082, NULL)
		lua_getupvalue =		xsc_port(1083, NULL)
		lua_setupvalue =		xsc_port(1084, NULL)
		lua_upvalueid =			xsc_port(1085, NULL)
		lua_upvaluejoin =		xsc_port(1086, NULL)
		lua_sethook =			xsc_port(1087, NULL)
		lua_gethook =			xsc_port(1088, NULL)
		lua_gethookmask =		xsc_port(1089, NULL)
		lua_gethookcount =		xsc_port(1090, NULL)
		' ID : 1501 to 1600 LUA函数 [内部库函数]
		luaopen_base =			xsc_port(1501, NULL)
		luaopen_coroutine =		xsc_port(1502, NULL)
		luaopen_table =			xsc_port(1503, NULL)
		luaopen_io =			xsc_port(1504, NULL)
		luaopen_os =			xsc_port(1505, NULL)
		luaopen_string =		xsc_port(1506, NULL)
		luaopen_utf8 =			xsc_port(1507, NULL)
		luaopen_bit32 =			xsc_port(1508, NULL)
		luaopen_math =			xsc_port(1509, NULL)
		luaopen_debug =			xsc_port(1510, NULL)
		luaopen_package =		xsc_port(1511, NULL)
		luaL_openlibs =			xsc_port(1512, NULL)
		' ID : 1601 to 2000 LUA函数 [封装函数]
		luaL_checkversion_ =	xsc_port(1601, NULL)
		luaL_getmetafield =		xsc_port(1602, NULL)
		luaL_callmeta =			xsc_port(1603, NULL)
		luaL_tolstring =		xsc_port(1604, NULL)
		luaL_argerror =			xsc_port(1605, NULL)
		luaL_checklstring =		xsc_port(1606, NULL)
		luaL_optlstring =		xsc_port(1607, NULL)
		luaL_checknumber =		xsc_port(1608, NULL)
		luaL_optnumber =		xsc_port(1609, NULL)
		luaL_checkinteger =		xsc_port(1610, NULL)
		luaL_optinteger =		xsc_port(1611, NULL)
		luaL_checkstack =		xsc_port(1612, NULL)
		luaL_checktype =		xsc_port(1613, NULL)
		luaL_checkany =			xsc_port(1614, NULL)
		luaL_newmetatable =		xsc_port(1615, NULL)
		luaL_setmetatable =		xsc_port(1616, NULL)
		luaL_testudata =		xsc_port(1617, NULL)
		luaL_checkudata =		xsc_port(1618, NULL)
		luaL_where =			xsc_port(1619, NULL)
		luaL_error =			xsc_port(1620, NULL)
		luaL_checkoption =		xsc_port(1621, NULL)
		luaL_fileresult =		xsc_port(1622, NULL)
		luaL_execresult =		xsc_port(1623, NULL)
		luaL_ref =				xsc_port(1624, NULL)
		luaL_unref =			xsc_port(1625, NULL)
		luaL_loadfilex =		xsc_port(1626, NULL)
		luaL_loadbufferx =		xsc_port(1627, NULL)
		luaL_loadstring =		xsc_port(1628, NULL)
		luaL_newstate =			xsc_port(1629, NULL)
		luaL_len =				xsc_port(1630, NULL)
		luaL_gsub =				xsc_port(1631, NULL)
		luaL_setfuncs =			xsc_port(1632, NULL)
		luaL_getsubtable =		xsc_port(1633, NULL)
		luaL_traceback =		xsc_port(1634, NULL)
		luaL_requiref =			xsc_port(1635, NULL)
		luaL_buffinit =			xsc_port(1636, NULL)
		luaL_prepbuffsize =		xsc_port(1637, NULL)
		luaL_addlstring =		xsc_port(1638, NULL)
		luaL_addstring =		xsc_port(1639, NULL)
		luaL_addvalue =			xsc_port(1640, NULL)
		luaL_pushresult =		xsc_port(1641, NULL)
		luaL_pushresultsize =	xsc_port(1642, NULL)
		luaL_buffinitsize =		xsc_port(1643, NULL)
	End Sub
#EndIf